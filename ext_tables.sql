#
# Table structure for table 'tx_courses_domain_model_course'
#
CREATE TABLE tx_courses_domain_model_course (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	insurance tinyint(1) unsigned DEFAULT '0' NOT NULL,
	courier_service tinyint(1) unsigned DEFAULT '0' NOT NULL,
	ranges int(11) unsigned DEFAULT '0' NOT NULL,
	locations int(11) unsigned DEFAULT '0' NOT NULL,
	exams int(11) unsigned DEFAULT '0' NOT NULL,
	media int(11) unsigned DEFAULT '0' NOT NULL,
	teaser varchar(255) DEFAULT '' NOT NULL,
	description mediumtext,
	discount varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sorting int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_location'
#
CREATE TABLE tx_courses_domain_model_location (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_accommodation'
#
CREATE TABLE tx_courses_domain_model_accommodation (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sorting int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_roomtype'
#
CREATE TABLE tx_courses_domain_model_roomtype (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sorting int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_food'
#
CREATE TABLE tx_courses_domain_model_food (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sorting int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_exam'
#
CREATE TABLE tx_courses_domain_model_exam (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sorting int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_transfer'
#
CREATE TABLE tx_courses_domain_model_transfer (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sorting int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_range'
#
CREATE TABLE tx_courses_domain_model_range (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	course int(11) unsigned DEFAULT '0' NOT NULL,

	mode int(11) DEFAULT '0' NOT NULL,
	value varchar(255) DEFAULT '' NOT NULL,
	price int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_metalocation'
#
CREATE TABLE tx_courses_domain_model_metalocation (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	course int(11) unsigned DEFAULT '0' NOT NULL,

	location int(11) unsigned DEFAULT '0',
	accommodations int(11) unsigned DEFAULT '0' NOT NULL,
	foods int(11) unsigned DEFAULT '0' NOT NULL,
	allowed_dates mediumtext,
	highseason_surcharge int(11) unsigned DEFAULT '0' NOT NULL,
	description mediumtext,
	transfers int(11) unsigned DEFAULT '0' NOT NULL,
	rooms int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_metaaccommodation'
#
CREATE TABLE tx_courses_domain_model_metaaccommodation (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	metalocation int(11) unsigned DEFAULT '0' NOT NULL,

	price int(11) DEFAULT '0' NOT NULL,
	day_surcharge int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,
	accommodation int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_metafood'
#
CREATE TABLE tx_courses_domain_model_metafood (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	metalocation int(11) unsigned DEFAULT '0' NOT NULL,

	price int(11) DEFAULT '0' NOT NULL,
	food int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_metatransfer'
#
CREATE TABLE tx_courses_domain_model_metatransfer (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	metalocation int(11) unsigned DEFAULT '0' NOT NULL,

	price int(11) DEFAULT '0' NOT NULL,
	transfer int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_metaexam'
#
CREATE TABLE tx_courses_domain_model_metaexam (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	course int(11) unsigned DEFAULT '0' NOT NULL,

	price int(11) DEFAULT '0' NOT NULL,
	exam int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_metaroom'
#
CREATE TABLE tx_courses_domain_model_metaroom (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	metalocation int(11) unsigned DEFAULT '0' NOT NULL,

	price int(11) DEFAULT '0' NOT NULL,
	room int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_order'
#
CREATE TABLE tx_courses_domain_model_order (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	first_name varchar(255) DEFAULT '' NOT NULL,
	last_name varchar(255) DEFAULT '' NOT NULL,
	salutation varchar(255) DEFAULT '' NOT NULL,
	company varchar(255) DEFAULT '' NOT NULL,
	work_for_company tinyint(1) unsigned DEFAULT '0' NOT NULL,
	street varchar(255) DEFAULT '' NOT NULL,
	zip varchar(255) DEFAULT '' NOT NULL,
	city varchar(255) DEFAULT '' NOT NULL,
	country varchar(255) DEFAULT '' NOT NULL,
	nationality varchar(255) DEFAULT '' NOT NULL,
	birthday int(11) DEFAULT '0' NOT NULL,
	passport_number varchar(255) DEFAULT '' NOT NULL,
	telephone varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	visa_required tinyint(1) unsigned DEFAULT '0' NOT NULL,
	learned_german tinyint(1) unsigned DEFAULT '0' NOT NULL,
	german_level varchar(255) DEFAULT '' NOT NULL,
	previous_course tinyint(1) unsigned DEFAULT '0' NOT NULL,
	previous_course_location varchar(255) DEFAULT '' NOT NULL,
	previous_course_year varchar(255) DEFAULT '' NOT NULL,
	allergies text NOT NULL,
	smoker tinyint(1) unsigned DEFAULT '0' NOT NULL,
	comment text NOT NULL,
	correspondence_language varchar(255) DEFAULT '' NOT NULL,
	known_from text NOT NULL,
	payment_type varchar(255) DEFAULT '' NOT NULL,
	course varchar(255) DEFAULT '' NOT NULL,
	location varchar(255) DEFAULT '' NOT NULL,
	accommodation varchar(255) DEFAULT '' NOT NULL,
	accommodation_before int(11) unsigned DEFAULT '0' NOT NULL,
	accommodation_after int(11) unsigned DEFAULT '0' NOT NULL,
	food varchar(255) DEFAULT '' NOT NULL,
	room varchar(255) DEFAULT '' NOT NULL,
	transfer varchar(255) DEFAULT '' NOT NULL,
	transfer_mode int(11) unsigned DEFAULT '0' NOT NULL,
	exam varchar(255) DEFAULT '' NOT NULL,
	ranges varchar(255) DEFAULT '' NOT NULL,
	price int(11) DEFAULT '0' NOT NULL,
	accommodation_range int(11) DEFAULT '0' NOT NULL,
	start_date varchar(255) DEFAULT '' NOT NULL,
	date_departure varchar(255) DEFAULT '' NOT NULL,
	date_arrival varchar(255) DEFAULT '' NOT NULL,
	agb tinyint(1) unsigned DEFAULT '0' NOT NULL,
	express_service tinyint(1) unsigned DEFAULT '0' NOT NULL,
	arrival_one_day_before tinyint(1) unsigned DEFAULT '0' NOT NULL,
	departure_one_day_after tinyint(1) unsigned DEFAULT '0' NOT NULL,
	insurance tinyint(1) unsigned DEFAULT '0' NOT NULL,
	insurance_start varchar(255) DEFAULT '' NOT NULL,
	insurance_end varchar(255) DEFAULT '' NOT NULL,
	additional_information text NOT NULL,
	agency text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_courses_domain_model_range'
#
CREATE TABLE tx_courses_domain_model_range (

	course  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_courses_domain_model_metalocation'
#
CREATE TABLE tx_courses_domain_model_metalocation (

	course  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_courses_domain_model_metaexam'
#
CREATE TABLE tx_courses_domain_model_metaexam (

	course  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_courses_domain_model_metaaccommodation'
#
CREATE TABLE tx_courses_domain_model_metaaccommodation (

	metalocation  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_courses_domain_model_metafood'
#
CREATE TABLE tx_courses_domain_model_metafood (

	metalocation  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_courses_domain_model_metatransfer'
#
CREATE TABLE tx_courses_domain_model_metatransfer (

	metalocation  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_courses_domain_model_metaroom'
#
CREATE TABLE tx_courses_domain_model_metaroom (

	metalocation  int(11) unsigned DEFAULT '0' NOT NULL,

);


#
# Table structure for table 'tx_courses_domain_model_request'
#
CREATE TABLE tx_courses_domain_model_request (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	salutation varchar(255) DEFAULT '' NOT NULL,
	first_name varchar(255) DEFAULT '' NOT NULL,
	last_name varchar(255) DEFAULT '' NOT NULL,
	telephone varchar(255) DEFAULT '' NOT NULL,
	country varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	course varchar(255) DEFAULT '' NOT NULL,
	location varchar(255) DEFAULT '' NOT NULL,
	accommodation varchar(255) DEFAULT '' NOT NULL,
	accommodation_before int(11) unsigned DEFAULT '0' NOT NULL,
	accommodation_after int(11) unsigned DEFAULT '0' NOT NULL,
	food varchar(255) DEFAULT '' NOT NULL,
	room varchar(255) DEFAULT '' NOT NULL,
	transfer varchar(255) DEFAULT '' NOT NULL,
	transfer_mode int(11) unsigned DEFAULT '0' NOT NULL,
	exam varchar(255) DEFAULT '' NOT NULL,
	ranges varchar(255) DEFAULT '' NOT NULL,
	price int(11) DEFAULT '0' NOT NULL,
	accommodation_range int(11) DEFAULT '0' NOT NULL,
	arrival_one_day_before tinyint(1) unsigned DEFAULT '0' NOT NULL,
	departure_one_day_after tinyint(1) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY language (l10n_parent,sys_language_uid)

);
