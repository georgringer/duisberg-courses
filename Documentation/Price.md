## Berechnung Kurs

- Der Kurs wird nach Woche berechnet.
- Bei vorhandener Hochsaison(HS) wird ein Zuschlag erhoben. (unabhängig je Woche 25€)
- Die Hochsaison ist je nach Jahr unterschiedlich (meist im Juli/August)
- Das Datum "Kursbeginn" sollte vorher ausgewählt werden könne.

## Berechung Unterkunft

- Die Unterkunft kann nie länger als der Kurs gebucht werden. (NEU!!!)
- Die Untekunft erhält ebenfalls einen HS Zuschlag. (unabhängig je Woche 25€)
- Die Unterkunft kann kürzer als der Kurs gebucht werden.
- Die Unterkunft wird mit dem ersten Kurstag gebucht.
- Sollte es einen Teilnehmer geben welcher vorher/nachher eine Unterkunft benötigt, wird dies nicht im Kurskonfigurator berechnet, hierzu wird der TN direkt mit unserem Kundenbetreuer telefonisch Kontakt aufnehmen. (NEU!!!)


## Hochsaison
Kurs(pro Woche 230 €) dauert 8 Wochen davon liegen 4 Wochen in der HS, dazu wird eine Unterkunft(pro Woche 200€) für 3 Wochen gebucht, davon liegen 2 Wochen in der HS.

- 230€ x 4 (4 Wochen normal)
- 230€ x 4 + 25€ x 4 (HS)
- 200€ x 1 (1 Woche Unterkunft normal)
- 200€ x 2 + 25€ x 2 (Unterkunft HS)
