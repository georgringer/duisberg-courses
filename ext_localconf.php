<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extKey) {

        $GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1433197759] = array(
            'nodeName' => 'json',
            'priority' => 40,
            'class' => \GeorgRinger\Courses\Hooks\Backend\Element\JsonElement::class,
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'GeorgRinger.' . $extKey,
            'Course',
            [
                'Form' => 'list, show',
                'Course' => 'list, show',
                'Location' => 'list',
                'Accommodation' => 'list, show',
                'RoomType' => 'list',
                'Exam' => 'list, show',
                'Transfer' => 'list, show',
                'Range' => 'list',
                'Order' => 'new,validate,create,success',
                'Request' => 'new, validate,create,success'

            ],
            // non-cacheable actions
            [
                'Form' => 'list,show',
                'Course' => '',
                'Location' => '',
                'Accommodation' => '',
                'RoomType' => '',
                'Exam' => '',
                'Transfer' => '',
                'Range' => '',
                'Order' => 'new,validate,create,success',
                'Request' => 'new, validate,create,success'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'GeorgRinger.' . $extKey,
            'Courselink',
            [
                'Course' => 'link',

            ],
            // non-cacheable actions
            [
            ]
        );

    },
    $_EXTKEY
);
