<?php
namespace GeorgRinger\Courses\Service;

use GeorgRinger\Courses\Domain\Model\Course;
use GeorgRinger\Courses\Domain\Model\MetaAccommodation;
use GeorgRinger\Courses\Domain\Model\MetaExam;
use GeorgRinger\Courses\Domain\Model\MetaFood;
use GeorgRinger\Courses\Domain\Model\MetaLocation;
use GeorgRinger\Courses\Domain\Model\MetaRoom;
use GeorgRinger\Courses\Domain\Model\MetaTransfer;
use GeorgRinger\Courses\Domain\Model\Range;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class RequestValidator
{
    const ADDITIONAL_SECRET = 'course_config';

    /**
     * @param Course $course
     * @param MetaLocation $location
     * @param MetaAccommodation|null $accommodation
     * @param MetaFood|null $food
     * @param MetaTransfer|null $transfer
     * @param MetaExam|null $exam
     * @param Range|null $range
     * @return string
     */
    public function generateHash(
        Course $course,
        MetaLocation $location,
        MetaAccommodation $accommodation = null,
        MetaFood $food = null,
        MetaTransfer $transfer = null,
        MetaExam $exam = null,
        Range $range = null
    ) {

        $data = [
            'course' => $course->getUid(),
            'location' => $location->getUid(),
            'accommodation' => $accommodation ? $accommodation->getUid() : 0,
            'food' => $food ? $food->getUid() : 0,
            'transfer' => $transfer ? $transfer->getUid() : 0,
            'exam' => $exam ? $exam->getUid() : 0,
            'range' => $range ? $range->getUid() : 0,
        ];

        $string = [];
        foreach ($data as $key => $value) {
            $string[] = $key . '-' . $value;
        }

        $keys = implode('_', $string);

        $final = $keys . '|' . GeneralUtility::hmac($keys, self::ADDITIONAL_SECRET);

        return $final;
    }

    /**
     * @param string $string
     * @return bool
     */
    public function validateHash($string)
    {
        $split = explode('|', $string, 2);

        if (count($split) !== 2) {
            return false;
        }

        return $split[1] === GeneralUtility::hmac($split[0], self::ADDITIONAL_SECRET);
    }

    public function check(
        Course $course,
        MetaLocation $location,
        MetaAccommodation $accommodation = null,
        MetaFood $food = null,
        MetaTransfer $transfer = null,
        MetaExam $exam = null,
        Range $range = null
    ) {
        $isValid = true;

        try {
            $this->childIsValid($course->getLocations(), $location);
            $this->childIsValid($location->getAccommodations(), $accommodation);
            $this->childIsValid($location->getFoods(), $food);
            $this->childIsValid($location->getTransfers(), $transfer);
            $this->childIsValid($course->getExams(), $exam);
            $this->childIsValid($course->getRanges(), $range);
        } catch (\Exception $e) {
            // @todo logging
            $isValid = false;
        }
        return $isValid;
    }

    protected function childIsValid($container, $child = null)
    {
        if (is_null($child) || is_null($container)) {
            return true;
        }
        foreach ($container as $item) {
            if ((int)$item->getUid() === (int)$child->getUid() && get_class($item) === get_class($child)) {
                return true;
            }
        }

        throw new \Exception(sprintf('Property %s is invalid!', get_class($child)));
    }
}
