<?php

namespace GeorgRinger\Courses\Service;

use DateTime;
use GeorgRinger\Courses\Domain\Model\MetaAccommodation;
use GeorgRinger\Courses\Domain\Model\MetaExam;
use GeorgRinger\Courses\Domain\Model\MetaFood;
use GeorgRinger\Courses\Domain\Model\MetaTransfer;
use GeorgRinger\Courses\Domain\Model\Range;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PriceService
{

    protected $debug = false;

    protected $courseTime;
    protected $highSeasonWeeks = [];
    protected $highSeasonSurcharge = 0;
    /** @var bool */
    protected $departureOneDayAfter = false;
    /** @var bool */
    protected $arrivalOneDayBefore = false;

    /** @var MetaFood */
    protected $food;

    /** @var  MetaAccommodation */
    protected $accommodation;

    /** @var  MetaTransfer */
    protected $transfer;

    /** @var MetaExam */
    protected $exam;

    /** @var  Range */
    protected $range;

    /** @var int */
    protected $transferMode;

    /** @var int */
    protected $accommodationRange;

    /** @var string */
    protected $discount;

    /** @var array */
    protected $information = [];

    public function getTotalPrice()
    {
        $total = 0;
        $range = $this->range;
        if (!is_null($range)) {
            // general course
            $course = $this->calculateCourse();
            $this->information['course'] = $course;
            $total += $course;

            $accommodation = $this->calculateAccommodation();
            $this->information['accommodation'] = $accommodation;
            $total += $accommodation;

            $exam = $this->calculateExam();
            $this->information['exam'] = $exam;
            $total += $exam;

            $transfer = $this->calculcateTransfer();
            $this->information['transfer'] = $transfer;
            $total += $transfer;

            $discount = $this->calculateDiscount();
            $this->information['discount'] = $discount;
            $total -=  $discount;
        }

        return $total;
    }

    protected function calculateCourse()
    {
        $priceOfSingleWeek = $this->range->getPrice();
        $price = $this->getDefaultPrice($this->range);
        $price = $this->calculateHighSeasonSurcharge('course', $priceOfSingleWeek, (int)$this->range->getValue());
        return $price;
    }

    protected function calculateAccommodation()
    {
        $price = 0;
        if ($this->accommodation) {
            $price = $this->getDefaultPrice($this->accommodation);
            $price = $this->multiplyPriceForWeeks($price, $this->accommodationRange);

            $surCharge = (int)$this->accommodation->getDaySurcharge();
            if ($surCharge > 0) {
                if ($this->arrivalOneDayBefore) {
                    $price += $surCharge;
                }
                if ($this->departureOneDayAfter) {
                    $price += $surCharge;
                }
            }
        }
        return $price;
    }

    protected function calculateExam()
    {
        $price = $this->getDefaultPrice($this->exam);
        return $price;
    }

    protected function calculcateTransfer()
    {
        $price = 0;
        if (!empty($this->transfer) && $this->transferMode !== null) {
            $price = $this->getDefaultPrice($this->transfer);

            if ((int)$this->transferMode === 1) {
                $price = $price * 2;
            }
        }

        return $price;
    }

    protected function multiplyPriceForWeeks($price, $overrideRange = null)
    {
        if (is_null($this->range)) {
            return $price;
        }
        $defaultRange = (int)$this->range->getValue();
        if ($overrideRange) {
            $defaultRange = (int)$overrideRange;
        }

        $price = $this->calculateHighSeasonSurcharge('accomodation', $price, $defaultRange);

        return $price;
    }

    protected function getDefaultPrice($object)
    {
        if (is_null($object)) {
            return 0;
        }
        $price = method_exists($object, 'getPriceReal') ? $object->getPriceReal() : $object->getPrice();
        return $price;
    }

    /**
     * @param mixed $courseTime
     */
    public function setCourseTime($courseTime)
    {
        $this->courseTime = $courseTime;
        return $this;
    }

    /**
     * @param array $highSeasonRange
     */
    public function setHighSeasonRange($highSeasonRange)
    {
        $this->highSeasonRange = $highSeasonRange;
        return $this;
    }

    /**
     * @param int $highSeasonSurcharge
     */
    public function setHighSeasonSurcharge($highSeasonSurcharge)
    {
        $this->highSeasonSurcharge = (int)$highSeasonSurcharge;
        return $this;
    }

    /**
     * @return MetaFood
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param MetaFood $food
     * @return PriceService
     */
    public function setFood($food)
    {
        $this->food = $food;
        return $this;
    }

    /**
     * @return MetaAccommodation
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    /**
     * @param MetaAccommodation $accommodation
     * @return PriceService
     */
    public function setAccommodation($accommodation)
    {
        $this->accommodation = $accommodation;
        return $this;
    }

    /**
     * @return MetaTransfer
     */
    public function getTransfer()
    {
        return $this->transfer;
    }

    /**
     * @param MetaTransfer $transfer
     * @return PriceService
     */
    public function setTransfer($transfer)
    {
        $this->transfer = $transfer;
        return $this;
    }

    /**
     * @return MetaExam
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * @param MetaExam $exam
     * @return PriceService
     */
    public function setExam($exam)
    {
        $this->exam = $exam;
        return $this;
    }

    /**
     * @return Range
     */
    public function getRange()
    {
        return $this->range;
    }

    /**
     * @param Range $range
     * @return PriceService
     */
    public function setRange($range)
    {
        $this->range = $range;
        return $this;
    }

    /**
     * @param mixed $transferMode
     * @return PriceService
     */
    public function setTransferMode($transferMode)
    {
        $this->transferMode = $transferMode;
        return $this;
    }

    /**
     * @param array $highSeasonWeeks
     * @return PriceService
     */
    public function setHighSeasonWeeks($highSeasonWeeks)
    {
        $this->highSeasonWeeks = $highSeasonWeeks;
        return $this;
    }

    /**
     * @param int $accommodationRange
     * @return PriceService
     */
    public function setAccommodationRange($accommodationRange)
    {
        $this->accommodationRange = (int)$accommodationRange;
        return $this;
    }


    /**
     * @param bool $departureOneDayAfter
     */
    public function setDepartureOneDayAfter($departureOneDayAfter)
    {
        $this->departureOneDayAfter = $departureOneDayAfter;
        return $this;
    }

    /**
     * @param bool $arrivalOneDayBefore
     */
    public function setArrivalOneDayBefore($arrivalOneDayBefore)
    {
        $this->arrivalOneDayBefore = $arrivalOneDayBefore;
        return $this;
    }


    /**
     * @param string $discount
     * @return PriceService
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @param string $type
     * @param $price
     * @param $defaultRange
     * @return mixed
     */
    protected function calculateHighSeasonSurcharge($type, $price, $defaultRange)
    {
        $courseStartDate = $this->courseTime;
        if (!$courseStartDate || !$this->range->isSurChargeSupported()) {
            switch ($this->range->getMode()) {
                case 1:
                case 2:
                    $price = $price * $defaultRange;
                    break;
                case 3:
                    // accomodation is always per week, even though it is pauschal
                    if ($type === 'accomodation') {
                        $price = $price * $defaultRange;
                    }
                    // nothing, price is "pauschal"
                    break;
                default:
                    die('nothing else supported');
            }
        } else {
            $weekOfCourse = DateTime::createFromFormat('d.m.Y', $courseStartDate);

            if ($weekOfCourse) {
                $weekOfCourse = $weekOfCourse->format('W');
                $weeksOfCourse = [];
                for ($i = 1; $i <= $defaultRange; $i++) {
                    $weeksOfCourse[] = $weekOfCourse + $i;
                }
                $highSeasonWeeks = GeneralUtility::trimExplode(',', $this->highSeasonWeeks, true);
                $countNonHighSeasonWeeks = count(array_diff($weeksOfCourse, $highSeasonWeeks));
                $countHighSeasonWeeks = $defaultRange - $countNonHighSeasonWeeks;

                if ($countHighSeasonWeeks < 0) {
                    $countHighSeasonWeeks = 0;
                }
                $firstPrice = $price;
                $price = ($price * $countNonHighSeasonWeeks) + (($price + $this->highSeasonSurcharge) * $countHighSeasonWeeks);
                $debug = [
                    'type' => $type,
                    'highSeasonWeeks' => $countHighSeasonWeeks,
                    'nonHighSeasonWeeks' => $countNonHighSeasonWeeks,
                    'surcharge' => $this->highSeasonSurcharge,
                    'singlePrice' => $firstPrice,
                    'finalPrice' => $price
                ];

                if ($this->debug) {
                    DebugUtility::debug($debug);
                }
            }

        }
        return $price;
    }

    protected function calculateDiscount()
    {
        $discount = 0;

        // check if discount syntax is valid;
        if (!$this->discount) {
            return $discount;
        }
        $discountSplit = GeneralUtility::trimExplode(':', $this->discount, true);
        if (count($discountSplit) !== 2) {
            return $discount;
        }

        $discountPrice = $discountSplit[0];
        $discountWeeks = GeneralUtility::trimExplode(',', $discountSplit[1], true);
        if (empty($discountWeeks)) {
            return $discount;
        }


        $range = (int)$this->range->getValue();

        $courseStartDate = $this->courseTime;
        if ($courseStartDate && $range) {
            $weekOfCourse = DateTime::createFromFormat('d.m.Y', $courseStartDate);

            if ($weekOfCourse) {
                $weekOfCourse = $weekOfCourse->format('W');
                $weeksOfCourse = [];
                for ($i = 1; $i <= $range; $i++) {
                    $weeksOfCourse[] = $weekOfCourse + $i;
                }

                $countDiscountWeeks = 0;
                foreach ($weeksOfCourse as $week) {
                    $week = (int)$week;
                    foreach ($discountWeeks as $dWeek) {
                        if ((int)$dWeek === $week) {
                            $countDiscountWeeks++;
                        }
                    }
                }

                if ($countDiscountWeeks) {
                    $discount = $countDiscountWeeks * $discountPrice;
                }
            }
        }

        if ($this->debug) {
            DebugUtility::debug([
                'discountWeeks' => $countDiscountWeeks,
                'discountTotal' => $discount
            ]);
        }

        return $discount;
    }

    /**
     * @return array
     */
    public function getPriceInformation()
    {
        return $this->information;
    }

}
