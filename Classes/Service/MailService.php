<?php

namespace GeorgRinger\Courses\Service;

use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

class MailService
{

    const ALTERNATIVE_PATH = 'EXT:courses/Resources/Private/Templates/Mail/';

    /**
     * Creates a fluid instance with given template-file
     *
     * @param array $arguments
     * @param string $template Path below Template-Root-Path
     * @param string $alternativePath
     * @param string $format
     * @return string
     */
    public function getFluidTemplate(
        array $arguments,
        $template,
        $alternativePath = '',
        $format = 'txt')
    {
        $alternativePath = $alternativePath ?: self::ALTERNATIVE_PATH;
        $alternativePath = trim($alternativePath, '/') . '/';
        /** @var StandaloneView $renderer */
        $renderer = GeneralUtility::makeInstance(StandaloneView::class);
        $renderer->setFormat($format);
        $path = GeneralUtility::getFileAbsFileName($alternativePath . $template);
        $renderer->setTemplatePathAndFilename($path);
        $renderer->assignMultiple($arguments);
        return trim($renderer->render());
    }

    public function getInlineFluidTemplate(array $arguments, $template)
    {
        $standaloneView = GeneralUtility::makeInstance(StandaloneView::class);
        $standaloneView->setTemplateSource($template);
        $standaloneView->assignMultiple($arguments);

        return trim($standaloneView->render());
    }

    public function sendMail(MailMessage $mailMessage, $plainContent, $htmlContent = '')
    {
        $mailMessage->setBody($plainContent)->setContentType('text/plain');
        if (!empty($htmlContent)) {
            $mailMessage->addPart($htmlContent, 'text/html');
        }
        return $mailMessage->send();
    }

}
