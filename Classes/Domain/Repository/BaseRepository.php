<?php
namespace GeorgRinger\Courses\Domain\Repository;

/*
 * This file is part of the courses extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use GeorgRinger\Courses\Domain\Model\Course;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;


/**
 * Base repository
 */
class BaseRepository extends Repository
{

    /**
     * @return DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return Course[]
     */
    public function findAll()
    {
        $query = $this->getQuery();
        return $query->execute();
    }

    /**
     * @return int
     */
    public function countAll()
    {
        $query = $this->getQuery();
        return $query->execute()->count();
    }

    /**
     * @return QueryInterface
     */
    protected function getQuery()
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);

        return $query;
    }

}