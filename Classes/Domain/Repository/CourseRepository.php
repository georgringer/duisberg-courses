<?php
namespace GeorgRinger\Courses\Domain\Repository;

    /*
     * This file is part of the courses extension for TYPO3 CMS.
     *
     * For the full copyright and license information, please read the
     * LICENSE.txt file that was distributed with this source code.
     */

/**
 * The repository for Courses
 */
class CourseRepository extends BaseRepository
{

    const IMAGE_MAX_WIDTH = 400;

    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );

    public function generateJson($expandToAllCombinations = false)
    {
        $json = $facetList = $courseTitles = [];
        $courses = $this->findAll();

        foreach ($courses as $course) {
            $courseTitles[$course->getTitle()] = $course->getTitle();
            $firstRange = $course->getFirstRange();
            $firstImage = $this->getImage($course->getMedia());

            foreach ($course->getLocations() as $location) {
                $baseItem = $jsonItem = [
                    'uid' => $course->getUid(),
                    'title' => $course->getTitle(),
                    'teaser' => $course->getTeaser(),
                    'description' => $course->getDescription(),
                    'firstImage' => $firstImage,
                    'basePrice' => $firstRange ? $firstRange->getPrice() : 0,
                    'location' => $location->getLocation()->getTitle(),
                    'location.uid' => $location->getUid(),
                ];
                $facet = [];
                $locationTitle = $location->getLocation()->getTitle();
                $facetList['location'][$locationTitle] = $locationTitle;

                foreach ($location->getAccommodations() as $accommodation) {
                    $item = $accommodation->getAccommodation();
                    if ($item) {
                        $title = $item->getTitle();
                        $facet[] = $facetList['accommodation'][$title] = $title;
                    }
                }
                $jsonItem['accommodation'] = $facet;
                $facet = [];
                foreach ($location->getFoods() as $food) {
                    $item = $food->getFood();
                    if ($item) {
                        $title = $item->getTitle();
                        $facet[] = $facetList['food'][$title] = $title;
                    }
                }
                $jsonItem['food'] = $facet;
                $facet = [];
//                foreach ($location->getRooms() as $room) {
//                    $item = $room->getRoom();
//                    if ($item) {
//                        $title = $item->getTitle();
//                        $facet[] = $facetList['room'][$title] = $title;
//                    }
//                }
                $facet = [];
//                foreach ($location->getTransfers() as $transfer) {
//                    $title = $transfer->getTransfer()->getTitle();
//                    $facet[] = $facetList['transfer'][$title] = $title;
//                }
//                $jsonItem['transfer'] = $facet;

                $variants = $full = [];

                $tmp = [];
                $i = 0;
                foreach ($location->getAccommodations() as $accommodation) {
                    $item = [
                        'key' => 'accommodation',
                        'price' => $accommodation->getPrice(),
                        'title' => $accommodation->getAccommodation()->getTitle(),
                        'uid' => $accommodation->getUid()
                    ];
                    $identifier = sprintf('acc_%s_%s', $accommodation->getUid(),
                        $accommodation->getAccommodation()->getTitle());
                    $full[$identifier] = $item;
                    $tmp[] = $identifier;
                }
                if (!empty($tmp)) {
                    $variants[$i] = $tmp;
                    $i++;
                }
                $tmp = [];
                foreach ($location->getFoods() as $food) {
                    if ($food->getFood()) {
                        $item = [
                            'key' => 'food',
                            'price' => $food->getPrice(),
                            'title' => $food->getFood()->getTitle(),
                            'uid' => $food->getUid()
                        ];
                        $identifier = sprintf('food_%s_%s', $food->getUid(),
                            $food->getFood()->getTitle());
                        $full[$identifier] = $item;
                        $tmp[] = $identifier;
                    }
                }
                if (!empty($tmp)) {
                    $variants[$i] = $tmp;
                    $i++;
                }
                $tmp = [];

                $tmp = [];
//                foreach ($location->getTransfers() as $transfer) {
//                    $item = [
//                        'key' => 'transfer',
//                        'price' => $transfer->getPrice(),
//                        'title' => $transfer->getTransfer()->getTitle(),
//                        'uid' => $transfer->getUid()
//                    ];
//                    $identifier = sprintf('transfer_%s_%s', $transfer->getUid(),
//                        $transfer->getTransfer()->getTitle());
//                    $full[$identifier] = $item;
//                    $tmp[] = $identifier;
//                }
//                if (!empty($tmp)) {
//                    $variants[$i] = $tmp;
//                }

                if ($expandToAllCombinations) {
                    $fun = $this->combos($variants);
                    foreach ($fun as $variant) {
                        $finalItem = $baseItem;
                        foreach ($variant as $key) {
                            $facet = $full[$key];
                            $label = $facet['key'];
                            $finalItem[$label] = $facet['title'];
                            $finalItem[$label . '.price'] = $facet['price'];
                            $finalItem[$label . '.uid'] = $facet['uid'];
                        }
//                    print_r($variants);
                        $json[] = $finalItem;
//                    print_R($finalItem);
//                    die;
                    }
                } else {

                    $json[] = $jsonItem;
                }


            }

        }

//                    print_r($json);
        return array('json' => $json, 'facetList' => $facetList, 'courseTitles' => $courseTitles);
    }

    protected function combos($data, &$all = array(), $group = array(), $val = null, $i = 0)
    {
        if (isset($val)) {
            array_push($group, $val);
        }

        if ($i >= count($data)) {
            array_push($all, $group);
        } else {
            if (!is_array($data[$i])) {
//                print_r($i);
//                print_r($data);
                die();
            }
            foreach ($data[$i] as $v) {
                $this->combos($data, $all, $group, $v, $i + 1);
            }
        }

        return $all;
    }

    /**
     * @param null $images
     * @return string
     */
    protected function getImage($images = null)
    {
        if ($images) {
            /** @var \TYPO3\CMS\Extbase\Domain\Model\FileReference $image */
            $image = null;
            foreach ($images as $image) {
                continue;
            }


            if (!is_null($image)) {
                $processingInstructions = array(
                    'maxWidth' => self::IMAGE_MAX_WIDTH
                );
                $processedImage = $this->imageService->applyProcessingInstructions($image->getOriginalResource(), $processingInstructions);
                $imageUri = $this->imageService->getImageUri($processedImage, true);
                return $imageUri;
            }
        }

        return '';
    }

    /**
     * @var \TYPO3\CMS\Extbase\Service\ImageService
     */
    protected $imageService;

    /**
     * @param \TYPO3\CMS\Extbase\Service\ImageService $imageService
     */
    public function injectImageService(\TYPO3\CMS\Extbase\Service\ImageService $imageService)
    {
        $this->imageService = $imageService;
    }
}