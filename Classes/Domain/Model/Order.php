<?php
namespace GeorgRinger\Courses\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Order
 */
class Order extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * firstName
     *
     * @var string
     * @validate NotEmpty
     */
    protected $firstName = '';

    /**
     * lastName
     *
     * @var string
     * @validate NotEmpty
     */
    protected $lastName = '';

    /**
     * salutation
     *
     * @var string
     * @validate NotEmpty
     */
    protected $salutation = '';

    /**
     * company
     *
     * @var string
     */
    protected $company = '';

    /**
     * workForCompany
     *
     * @var bool
     */
    protected $workForCompany = false;

    /**
     * street
     *
     * @var string
     * @validate NotEmpty
     */
    protected $street = '';

    /**
     * zip
     *
     * @var string
     * @validate NotEmpty
     */
    protected $zip = '';

    /**
     * city
     *
     * @var string
     * @validate NotEmpty
     */
    protected $city = '';

    /**
     * country
     *
     * @var string
     * @validate NotEmpty
     */
    protected $country = '';

    /**
     * nationality
     *
     * @var string
     * @validate NotEmpty
     */
    protected $nationality = '';

    /**
     * birthday
     *
     * @var \DateTime
     * @validate NotEmpty
     */
    protected $birthday = null;

    /**
     * passportNumber
     *
     * @var string
     */
    protected $passportNumber = '';

    /**
     * telephone
     *
     * @var string
     */
    protected $telephone = '';

    /**
     * email
     *
     * @var string
     * @validate NotEmpty
     * @validate EmailAddress
     */
    protected $email = '';

    /**
     * visaRequired
     *
     * @var bool
     * @validate NotEmpty
     */
    protected $visaRequired = false;

    /**
     * learnedGerman
     *
     * @var bool
     */
    protected $learnedGerman = false;

    /**
     * germanLevel
     *
     * @var string
     */
    protected $germanLevel = '';

    /**
     * previousCourse
     *
     * @var bool
     */
    protected $previousCourse = false;

    /**
     * previousCourseLocation
     *
     * @var string
     */
    protected $previousCourseLocation = '';

    /**
     * previousCourseYear
     *
     * @var string
     */
    protected $previousCourseYear = '';

    /**
     * allergies
     *
     * @var string
     */
    protected $allergies = '';

    /**
     * smoker
     *
     * @var bool
     */
    protected $smoker = false;

    /**
     * comment
     *
     * @var string
     */
    protected $comment = '';

    /**
     * correspondenceLanguage
     *
     * @var string
     * @validate NotEmpty
     */
    protected $correspondenceLanguage = '';

    /**
     * knownFrom
     *
     * @var string
     */
    protected $knownFrom = '';

    /**
     * paymentType
     *
     * @var string
     */
    protected $paymentType = '';

    /**
     * course
     *
     * @var string
     */
    protected $course = '';

    /**
     * location
     *
     * @var string
     */
    protected $location = '';

    /**
     * accommodation
     *
     * @var string
     */
    protected $accommodation = '';

    /**
     * food
     *
     * @var string
     */
    protected $food = '';

    /**
     * room
     *
     * @var string
     */
    protected $room = '';

    /**
     * transfer
     *
     * @var string
     */
    protected $transfer = '';

    /** @var int */
    protected $transferMode = 0;

    /**
     * exam
     *
     * @var array
     */
    protected $exam = [];

    /**
     * ranges
     *
     * @var string
     */
    protected $ranges = '';

    /** @var string */
    protected $dateArrival = '';

    /** @var string */
    protected $dateDeparture = '';

    /** @var string */
    protected $insuranceStart = '';
    /** @var string */
    protected $insuranceEnd = '';
    /** @var bool */
    protected $insurance = false;
    /** @var bool */
    protected $expressService = false;
    /**
     * @var bool
     * @validate Boolean(is=true)
     */
    protected $agb = false;

    /** @var string */
    protected $additionalInformation = '';
    /**
     * price
     *
     * @var int
     */
    protected $price = 0;

    /** @var int */
    protected $accommodationRange;

    /** @var string */
    protected $startDate;

    /** @var int */
    protected $accommodationBefore;

    /** @var int */
    protected $accommodationAfter;

    /**
     * @var bool
     */
    protected $arrivalOneDayBefore = false;

    /**
     * @var bool
     */
    protected $departureOneDayAfter = false;

    /** @var string */
    protected $agency;

    /**
     * Returns the firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the firstName
     *
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Returns the lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets the lastName
     *
     * @param string $lastName
     * @return void
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Returns the salutation
     *
     * @return string $salutation
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * Sets the salutation
     *
     * @param string $salutation
     * @return void
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * Returns the company
     *
     * @return string $company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the company
     *
     * @param string $company
     * @return void
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return bool
     */
    public function isWorkForCompany()
    {
        return $this->workForCompany;
    }
    /**
     * @return bool
     */
    public function getWorkForCompany()
    {
        return $this->workForCompany;
    }


    /**
     * @param bool $workForCompany
     */
    public function setWorkForCompany($workForCompany)
    {
        $this->workForCompany = $workForCompany;
    }


    /**
     * Returns the street
     *
     * @return string $street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Sets the street
     *
     * @param string $street
     * @return void
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * Returns the zip
     *
     * @return string $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param string $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param string $country
     * @return void
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Returns the nationality
     *
     * @return string $nationality
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Sets the nationality
     *
     * @param string $nationality
     * @return void
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * Returns the birthday
     *
     * @return \DateTime $birthday
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Sets the birthday
     *
     * @param \DateTime $birthday
     * @return void
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * Returns the passportNumber
     *
     * @return string $passportNumber
     */
    public function getPassportNumber()
    {
        return $this->passportNumber;
    }

    /**
     * Sets the passportNumber
     *
     * @param string $passportNumber
     * @return void
     */
    public function setPassportNumber($passportNumber)
    {
        $this->passportNumber = $passportNumber;
    }

    /**
     * Returns the telephone
     *
     * @return string $telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Sets the telephone
     *
     * @param string $telephone
     * @return void
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the visaRequired
     *
     * @return bool $visaRequired
     */
    public function getVisaRequired()
    {
        return $this->visaRequired;
    }

    /**
     * Sets the visaRequired
     *
     * @param bool $visaRequired
     * @return void
     */
    public function setVisaRequired($visaRequired)
    {
        $this->visaRequired = $visaRequired;
    }

    /**
     * Returns the boolean state of visaRequired
     *
     * @return bool
     */
    public function isVisaRequired()
    {
        return $this->visaRequired;
    }

    /**
     * Returns the learnedGerman
     *
     * @return bool $learnedGerman
     */
    public function getLearnedGerman()
    {
        return $this->learnedGerman;
    }

    /**
     * Sets the learnedGerman
     *
     * @param bool $learnedGerman
     * @return void
     */
    public function setLearnedGerman($learnedGerman)
    {
        $this->learnedGerman = $learnedGerman;
    }

    /**
     * Returns the boolean state of learnedGerman
     *
     * @return bool
     */
    public function isLearnedGerman()
    {
        return $this->learnedGerman;
    }

    /**
     * Returns the germanLevel
     *
     * @return string $germanLevel
     */
    public function getGermanLevel()
    {
        return $this->germanLevel;
    }

    /**
     * Sets the germanLevel
     *
     * @param string $germanLevel
     * @return void
     */
    public function setGermanLevel($germanLevel)
    {
        $this->germanLevel = $germanLevel;
    }

    /**
     * Returns the previousCourse
     *
     * @return bool $previousCourse
     */
    public function getPreviousCourse()
    {
        return $this->previousCourse;
    }

    /**
     * Sets the previousCourse
     *
     * @param bool $previousCourse
     * @return void
     */
    public function setPreviousCourse($previousCourse)
    {
        $this->previousCourse = $previousCourse;
    }

    /**
     * Returns the boolean state of previousCourse
     *
     * @return bool
     */
    public function isPreviousCourse()
    {
        return $this->previousCourse;
    }

    /**
     * Returns the previousCourseLocation
     *
     * @return string $previousCourseLocation
     */
    public function getPreviousCourseLocation()
    {
        return $this->previousCourseLocation;
    }

    /**
     * Sets the previousCourseLocation
     *
     * @param string $previousCourseLocation
     * @return void
     */
    public function setPreviousCourseLocation($previousCourseLocation)
    {
        $this->previousCourseLocation = $previousCourseLocation;
    }

    /**
     * Returns the previousCourseYear
     *
     * @return string $previousCourseYear
     */
    public function getPreviousCourseYear()
    {
        return $this->previousCourseYear;
    }

    /**
     * Sets the previousCourseYear
     *
     * @param string $previousCourseYear
     * @return void
     */
    public function setPreviousCourseYear($previousCourseYear)
    {
        $this->previousCourseYear = $previousCourseYear;
    }

    /**
     * Returns the allergies
     *
     * @return string $allergies
     */
    public function getAllergies()
    {
        return $this->allergies;
    }

    /**
     * Sets the allergies
     *
     * @param string $allergies
     * @return void
     */
    public function setAllergies($allergies)
    {
        $this->allergies = $allergies;
    }

    /**
     * Returns the smoker
     *
     * @return bool $smoker
     */
    public function getSmoker()
    {
        return $this->smoker;
    }

    /**
     * Sets the smoker
     *
     * @param bool $smoker
     * @return void
     */
    public function setSmoker($smoker)
    {
        $this->smoker = $smoker;
    }

    /**
     * Returns the boolean state of smoker
     *
     * @return bool
     */
    public function isSmoker()
    {
        return $this->smoker;
    }

    /**
     * Returns the comment
     *
     * @return string $comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Sets the comment
     *
     * @param string $comment
     * @return void
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Returns the correspondenceLanguage
     *
     * @return string $correspondenceLanguage
     */
    public function getCorrespondenceLanguage()
    {
        return $this->correspondenceLanguage;
    }

    /**
     * Sets the correspondenceLanguage
     *
     * @param string $correspondenceLanguage
     * @return void
     */
    public function setCorrespondenceLanguage($correspondenceLanguage)
    {
        $this->correspondenceLanguage = $correspondenceLanguage;
    }

    /**
     * Returns the knownFrom
     *
     * @return string $knownFrom
     */
    public function getKnownFrom()
    {
        return $this->knownFrom;
    }

    /**
     * Sets the knownFrom
     *
     * @param string $knownFrom
     * @return void
     */
    public function setKnownFrom($knownFrom)
    {
        $this->knownFrom = $knownFrom;
    }

    /**
     * Returns the paymentType
     *
     * @return string $paymentType
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Sets the paymentType
     *
     * @param string $paymentType
     * @return void
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * Returns the course
     *
     * @return string $course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Sets the course
     *
     * @param string $course
     * @return void
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }

    /**
     * Returns the location
     *
     * @return string $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location
     *
     * @param string $location
     * @return void
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Returns the accommodation
     *
     * @return string $accommodation
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    /**
     * Sets the accommodation
     *
     * @param string $accommodation
     * @return void
     */
    public function setAccommodation($accommodation)
    {
        $this->accommodation = $accommodation;
    }

    /**
     * Returns the food
     *
     * @return string $food
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * Sets the food
     *
     * @param string $food
     * @return void
     */
    public function setFood($food)
    {
        $this->food = $food;
    }

    /**
     * Returns the room
     *
     * @return string $room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Sets the room
     *
     * @param string $room
     * @return void
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * Returns the transfer
     *
     * @return string $transfer
     */
    public function getTransfer()
    {
        return $this->transfer;
    }

    /**
     * Sets the transfer
     *
     * @param string $transfer
     * @return void
     */
    public function setTransfer($transfer)
    {
        $this->transfer = $transfer;
    }

    /**
     * Returns the exam
     *
     * @return array $exam
     */
    public function getExam()
    {
        return (array)$this->exam;
    }

    /**
     * Sets the exam
     *
     * @param string $exam
     * @return void
     */
    public function setExam($exam)
    {
        $this->exam = $exam;
    }

    /**
     * Returns the ranges
     *
     * @return string $ranges
     */
    public function getRanges()
    {
        return $this->ranges;
    }

    /**
     * Sets the ranges
     *
     * @param string $ranges
     * @return void
     */
    public function setRanges($ranges)
    {
        $this->ranges = $ranges;
    }

    /**
     * Sets the ranges
     *
     * @param string $ranges
     * @return void
     */
    public function setRange($ranges)
    {
        $this->ranges = $ranges;
    }

    /**
     * Returns the price
     *
     * @return int $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the price
     *
     * @param int $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getAccommodationBefore()
    {
        return $this->accommodationBefore;
    }

    /**
     * @param int $accommodationBefore
     */
    public function setAccommodationBefore($accommodationBefore)
    {
        $this->accommodationBefore = $accommodationBefore;
    }

    /**
     * @return int
     */
    public function getAccommodationAfter()
    {
        return $this->accommodationAfter;
    }

    /**
     * @param int $accommodationAfter
     */
    public function setAccommodationAfter($accommodationAfter)
    {
        $this->accommodationAfter = $accommodationAfter;
    }

    public function getCourseEncoded() {
        return $this->getEncodedData('course');
    }

    public function getLocationEncoded() {
        return $this->getEncodedData('location');
    }

    public function getAccommodationEncoded() {
        return $this->getEncodedData('accommodation');
    }

    public function getFoodEncoded() {
        return $this->getEncodedData('food');
    }

    public function getRoomEncoded() {
        return $this->getEncodedData('room');
    }

    public function getTransferEncoded() {
        return $this->getEncodedData('transfer');
    }

    public function getExamEncoded() {
        return $this->getEncodedData('exam');
    }

    public function getRangeEncoded() {
        return $this->getEncodedData('ranges');
    }

    protected function getEncodedData($field) {
        return json_decode($this->$field, true);
    }

    /**
     * @return int
     */
    public function getTransferMode()
    {
        return $this->transferMode;
    }

    /**
     * @param int $transferMode
     */
    public function setTransferMode($transferMode)
    {
        $this->transferMode = $transferMode;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return int
     */
    public function getAccommodationRange()
    {
        return $this->accommodationRange;
    }

    /**
     * @param int $accommodationRange
     */
    public function setAccommodationRange($accommodationRange)
    {
        $this->accommodationRange = $accommodationRange;
    }

    /**
     * @return string
     */
    public function getDateArrival()
    {
        return $this->dateArrival;
    }

    /**
     * @param string $dateArrival
     */
    public function setDateArrival($dateArrival)
    {
        $this->dateArrival = $dateArrival;
    }

    /**
     * @return string
     */
    public function getDateDeparture()
    {
        return $this->dateDeparture;
    }

    /**
     * @param string $dateDeparture
     */
    public function setDateDeparture($dateDeparture)
    {
        $this->dateDeparture = $dateDeparture;
    }

    /**
     * @return string
     */
    public function getInsuranceStart()
    {
        return $this->insuranceStart;
    }

    /**
     * @param string $insuranceStart
     */
    public function setInsuranceStart($insuranceStart)
    {
        $this->insuranceStart = $insuranceStart;
    }

    /**
     * @return string
     */
    public function getInsuranceEnd()
    {
        return $this->insuranceEnd;
    }

    /**
     * @param string $insuranceEnd
     */
    public function setInsuranceEnd($insuranceEnd)
    {
        $this->insuranceEnd = $insuranceEnd;
    }

    /**
     * @return bool
     */
    public function isInsurance()
    {
        return $this->insurance;
    }

    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * @param bool $insurance
     */
    public function setInsurance($insurance)
    {
        $this->insurance = $insurance;
    }

    /**
     * @return bool
     */
    public function isExpressService()
    {
        return $this->expressService;
    }

    /**
     * @return bool
     */
    public function getExpressService()
    {
        return $this->expressService;
    }


    /**
     * @param bool $expressService
     */
    public function setExpressService($expressService)
    {
        $this->expressService = $expressService;
    }

    /**
     * @return bool
     */
    public function isAgb()
    {
        return $this->agb;
    }
    public function getAgb()
    {
        return $this->agb;
    }

    /**
     * @param bool $agb
     */
    public function setAgb($agb)
    {
        $this->agb = $agb;
    }

    /**
     * @return string
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * @param string $additionalInformation
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;
    }

    public function getAccommodationRangeInsurance() {
        return (int)$this->getAccommodationRange() * 20;
    }

    /**
     * @return bool
     */
    public function getArrivalOneDayBefore()
    {
        return $this->arrivalOneDayBefore;
    }

    /**
     * @param bool $arrivalOneDayBefore
     */
    public function setArrivalOneDayBefore($arrivalOneDayBefore)
    {
        $this->arrivalOneDayBefore = $arrivalOneDayBefore;
    }

    /**
     * @return bool
     */
    public function getDepartureOneDayAfter()
    {
        return $this->departureOneDayAfter;
    }

    /**
     * @param bool $departureOneDayAfter
     */
    public function setDepartureOneDayAfter($departureOneDayAfter)
    {
        $this->departureOneDayAfter = $departureOneDayAfter;
    }

    /**
     * @return string
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * @param string $agency
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;
    }

}
