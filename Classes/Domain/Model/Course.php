<?php
namespace GeorgRinger\Courses\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Course
 */
class Course extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * insurance
     *
     * @var bool
     */
    protected $insurance = false;

    /**
     * courierService
     *
     * @var bool
     */
    protected $courierService = false;

    /**
     * ranges
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\Range>
     * @cascade remove
     * @lazy
     */
    protected $ranges = null;

    /**
     * locations
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaLocation>
     * @cascade remove
     */
    protected $locations = null;

    /**
     * exams
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaExam>
     * @cascade remove
     */
    protected $exams = null;

    /**
     * @var string
     */
    protected $teaser;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $media;

    /** @var string */
    protected $discount;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->ranges = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->locations = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->exams = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the insurance
     *
     * @return bool $insurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Sets the insurance
     *
     * @param bool $insurance
     * @return void
     */
    public function setInsurance($insurance)
    {
        $this->insurance = $insurance;
    }

    /**
     * Returns the boolean state of insurance
     *
     * @return bool
     */
    public function isInsurance()
    {
        return $this->insurance;
    }

    /**
     * Returns the courierService
     *
     * @return bool $courierService
     */
    public function getCourierService()
    {
        return $this->courierService;
    }

    /**
     * Sets the courierService
     *
     * @param bool $courierService
     * @return void
     */
    public function setCourierService($courierService)
    {
        $this->courierService = $courierService;
    }

    /**
     * Returns the boolean state of courierService
     *
     * @return bool
     */
    public function isCourierService()
    {
        return $this->courierService;
    }

    /**
     * Adds a Range
     *
     * @param \GeorgRinger\Courses\Domain\Model\Range $range
     * @return void
     */
    public function addRange(\GeorgRinger\Courses\Domain\Model\Range $range)
    {
        $this->ranges->attach($range);
    }

    /**
     * Removes a Range
     *
     * @param \GeorgRinger\Courses\Domain\Model\Range $rangeToRemove The Range to be removed
     * @return void
     */
    public function removeRange(\GeorgRinger\Courses\Domain\Model\Range $rangeToRemove)
    {
        $this->ranges->detach($rangeToRemove);
    }

    /**
     * Returns the ranges
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\Range> $ranges
     */
    public function getRanges()
    {
        return $this->ranges;
    }

    /**
     * Sets the ranges
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\Range> $ranges
     * @return void
     */
    public function setRanges(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $ranges)
    {
        $this->ranges = $ranges;
    }

    /**
     * Adds a MetaLocation
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaLocation $location
     * @return void
     */
    public function addLocation(\GeorgRinger\Courses\Domain\Model\MetaLocation $location)
    {
        $this->locations->attach($location);
    }

    /**
     * Removes a MetaLocation
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaLocation $locationToRemove The MetaLocation to be removed
     * @return void
     */
    public function removeLocation(\GeorgRinger\Courses\Domain\Model\MetaLocation $locationToRemove)
    {
        $this->locations->detach($locationToRemove);
    }

    /**
     * Returns the locations
     *
     * @return \GeorgRinger\Courses\Domain\Model\MetaLocation[]
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Sets the locations
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaLocation> $locations
     * @return void
     */
    public function setLocations(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $locations)
    {
        $this->locations = $locations;
    }

    /**
     * Adds a MetaExam
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaExam $exam
     * @return void
     */
    public function addExam(\GeorgRinger\Courses\Domain\Model\MetaExam $exam)
    {
        $this->exams->attach($exam);
    }

    /**
     * Removes a MetaExam
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaExam $examToRemove The MetaExam to be removed
     * @return void
     */
    public function removeExam(\GeorgRinger\Courses\Domain\Model\MetaExam $examToRemove)
    {
        $this->exams->detach($examToRemove);
    }

    /**
     * Returns the exams
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaExam> $exams
     */
    public function getExams()
    {
        return $this->exams;
    }

    /**
     * Sets the exams
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaExam> $exams
     * @return void
     */
    public function setExams(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $exams)
    {
        $this->exams = $exams;
    }

    // various getters

    /**
     * @return Range
     */
    public function getFirstRange() {
        foreach($this->ranges as $range) {
            return $range;
        }
        return null;
    }

    /**
     * @return Range
     */
    public function getLastRange() {
        $lastRange = null;
        foreach($this->ranges as $range) {
            $lastRange = $range;
        }
        return $lastRange;
    }


    /**
     * @return string
     */
    public function getTeaser()
    {
        return $this->teaser;
    }

    /**
     * @param string $teaser
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $media
     */
    public function setMedia($media)
    {
        $this->media = $media;
    }

    /**
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param string $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

}
