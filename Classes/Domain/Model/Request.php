<?php

namespace GeorgRinger\Courses\Domain\Model;

/***
 *
 * This file is part of the "Course Configuration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Request
 */
class Request extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * salutation
     *
     * @var string
     */
    protected $salutation = '';

    /**
     * firstName
     *
     * @var string
     */
    protected $firstName = '';

    /**
     * lastName
     *
     * @var string
     */
    protected $lastName = '';

    /**
     * telephone
     *
     * @var string
     */
    protected $telephone = '';

    /**
     * country
     *
     * @var string
     */
    protected $country = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';


    /**
     * course
     *
     * @var string
     */
    protected $course = '';

    /**
     * location
     *
     * @var string
     */
    protected $location = '';

    /**
     * accommodation
     *
     * @var string
     */
    protected $accommodation = '';

    /**
     * food
     *
     * @var string
     */
    protected $food = '';

    /**
     * room
     *
     * @var string
     */
    protected $room = '';

    /**
     * transfer
     *
     * @var string
     */
    protected $transfer = '';

    /** @var int */
    protected $transferMode = 0;

    /**
     * exam
     *
     * @var array
     */
    protected $exam = [];

    /**
     * ranges
     *
     * @var string
     */
    protected $ranges = '';

    /** @var int */
    protected $accommodationRange;

    /** @var int */
    protected $accommodationBefore;

    /** @var int */
    protected $accommodationAfter;

    /**
     * @var bool
     */
    protected $arrivalOneDayBefore = false;

    /**
     * @var bool
     */
    protected $departureOneDayAfter = false;

    /**
     * price
     *
     * @var int
     */
    protected $price = 0;

    /**
     * Returns the salutation
     *
     * @return string $salutation
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * Sets the salutation
     *
     * @param string $salutation
     * @return void
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * Returns the firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the firstName
     *
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Returns the lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets the lastName
     *
     * @param string $lastName
     * @return void
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Returns the telephone
     *
     * @return string $telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Sets the telephone
     *
     * @param string $telephone
     * @return void
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * Returns the country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param string $country
     * @return void
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param string $course
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    /**
     * @param string $accommodation
     */
    public function setAccommodation($accommodation)
    {
        $this->accommodation = $accommodation;
    }

    /**
     * @return string
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param string $food
     */
    public function setFood($food)
    {
        $this->food = $food;
    }

    /**
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param string $room
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * @return string
     */
    public function getTransfer()
    {
        return $this->transfer;
    }

    /**
     * @param string $transfer
     */
    public function setTransfer($transfer)
    {
        $this->transfer = $transfer;
    }

    /**
     * @return int
     */
    public function getTransferMode()
    {
        return $this->transferMode;
    }

    /**
     * @param int $transferMode
     */
    public function setTransferMode($transferMode)
    {
        $this->transferMode = $transferMode;
    }

    /**
     * @return array
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * @param array $exam
     */
    public function setExam($exam)
    {
        $this->exam = $exam;
    }

    /**
     * @return string
     */
    public function getRanges()
    {
        return $this->ranges;
    }

    /**
     * @param string $ranges
     */
    public function setRanges($ranges)
    {
        $this->ranges = $ranges;
    }

    /**
     * @return int
     */
    public function getAccommodationRange()
    {
        return $this->accommodationRange;
    }

    /**
     * @param int $accommodationRange
     */
    public function setAccommodationRange($accommodationRange)
    {
        $this->accommodationRange = $accommodationRange;
    }

    /**
     * @return int
     */
    public function getAccommodationBefore()
    {
        return $this->accommodationBefore;
    }

    /**
     * @param int $accommodationBefore
     */
    public function setAccommodationBefore($accommodationBefore)
    {
        $this->accommodationBefore = $accommodationBefore;
    }

    /**
     * @return int
     */
    public function getAccommodationAfter()
    {
        return $this->accommodationAfter;
    }

    /**
     * @param int $accommodationAfter
     */
    public function setAccommodationAfter($accommodationAfter)
    {
        $this->accommodationAfter = $accommodationAfter;
    }

    /**
     * @return bool
     */
    public function getArrivalOneDayBefore()
    {
        return $this->arrivalOneDayBefore;
    }

    /**
     * @param bool $arrivalOneDayBefore
     */
    public function setArrivalOneDayBefore($arrivalOneDayBefore)
    {
        $this->arrivalOneDayBefore = $arrivalOneDayBefore;
    }

    /**
     * @return bool
     */
    public function getDepartureOneDayAfter()
    {
        return $this->departureOneDayAfter;
    }

    /**
     * @param bool $departureOneDayAfter
     */
    public function setDepartureOneDayAfter($departureOneDayAfter)
    {
        $this->departureOneDayAfter = $departureOneDayAfter;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Sets the ranges
     *
     * @param string $ranges
     * @return void
     */
    public function setRange($ranges)
    {
        $this->ranges = $ranges;
    }
}
