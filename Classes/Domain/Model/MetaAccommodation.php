<?php
namespace GeorgRinger\Courses\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * MetaAccommodation
 */
class MetaAccommodation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * price
     *
     * @var int
     */
    protected $price = 0;

    /**
     * @var int
     */
    protected $daySurcharge = 0;

    /**
     * accommodation
     *
     * @var \GeorgRinger\Courses\Domain\Model\Accommodation
     */
    protected $accommodation = null;

    /**
     * Returns the price
     *
     * @return int $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the price
     *
     * @param int $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Returns the accommodation
     *
     * @return \GeorgRinger\Courses\Domain\Model\Accommodation $accommodation
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    /**
     * Sets the accommodation
     *
     * @param \GeorgRinger\Courses\Domain\Model\Accommodation $accommodation
     * @return void
     */
    public function setAccommodation(\GeorgRinger\Courses\Domain\Model\Accommodation $accommodation)
    {
        $this->accommodation = $accommodation;
    }

    /**
     * @return int
     */
    public function getDaySurcharge()
    {
        return $this->daySurcharge;
    }

    /**
     * @param int $daySurcharge
     */
    public function setDaySurcharge($daySurcharge)
    {
        $this->daySurcharge = $daySurcharge;
    }

}