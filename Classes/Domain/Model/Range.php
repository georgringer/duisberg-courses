<?php
namespace GeorgRinger\Courses\Domain\Model;


    /***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2016
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * Range
 */
class Range extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * mode
     *
     * @var int
     */
    protected $mode = 0;

    /**
     * value
     *
     * @var string
     */
    protected $value = '';

    /**
     * price
     *
     * @var int
     */
    protected $price = 0;

    /**
     * Returns the mode
     *
     * @return int $mode
     */
    public function getMode()
    {
        return (int)$this->mode;
    }

    /**
     * Sets the mode
     *
     * @param int $mode
     * @return void
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * Returns the value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the value
     *
     * @param string $value
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Returns the price
     *
     * @return int $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the price
     *
     * @param int $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function isSurChargeSupported() {
        return (int)$this->mode === 1;
    }

    public function getPriceReal()
    {
        switch ((int)$this->mode) {
            // week
            case 1:
                return $this->price * $this->value;
                break;
            // TE: same as week
            case 2:
                return $this->price * $this->value;
                break;
            // pauschal
            case 3:
                return $this->price;
            default:
                // @todo finalize it
                throw new \RuntimeException(sprintf('Mode %s for price calculcation not yet implemented', $this->mode));
        }
    }

    public function getTitle()
    {
        return $this->getMode();
    }

}