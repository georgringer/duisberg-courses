<?php
namespace GeorgRinger\Courses\Domain\Model\Dto;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

class SearchDemand
{

    /** @var int */
    protected $room;

    /** @var int */
    protected $accommodation;

    /** @var  int */
    protected $accommodationBefore;

    /** @var  int */
    protected $accommodationAfter;

    /** @var int */
    protected $food;

    /** @var int */
    protected $transfer;

    /** @var int */
    protected $exam;

    /** @var int */
    protected $range;

    /** @var int */
    protected $transferMode;

    /** @var string */
    protected $startDate;

    /** @var int */
    protected $accommodationRange;

    /** @var int */
    protected $course;

    /** @var int */
    protected $location;

    /**
     * @var bool
     * @transient
     */
    protected $arrivalOneDayBefore;

    /**
     * @var bool
     * @transient
     */
    protected $departureOneDayAfter;

    /**
     * @return int
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param int $room
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * @return int
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * @param int $food
     */
    public function setFood($food)
    {
        $this->food = $food;
    }

    /**
     * @return int
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    /**
     * @param int $accommodation
     */
    public function setAccommodation($accommodation)
    {
        $this->accommodation = $accommodation;
    }

    /**
     * @return int
     */
    public function getTransfer()
    {
        return $this->transfer;
    }

    /**
     * @param int $transfer
     */
    public function setTransfer($transfer)
    {
        $this->transfer = $transfer;
    }

    /**
     * @return int
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * @param int $exam
     */
    public function setExam($exam)
    {
        $this->exam = $exam;
    }

    /**
     * @return int
     */
    public function getRange()
    {
        return $this->range;
    }

    /**
     * @param int $range
     */
    public function setRange($range)
    {
        $this->range = $range;
    }

    /**
     * @return int
     */
    public function getTransferMode()
    {
        return $this->transferMode;
    }

    /**
     * @param int $transferMode
     */
    public function setTransferMode($transferMode)
    {
        $this->transferMode = $transferMode;
    }

    /**
     * @return int
     */
    public function getAccommodationBefore()
    {
        return $this->accommodationBefore;
    }

    /**
     * @param int $accommodationBefore
     */
    public function setAccommodationBefore($accommodationBefore)
    {
        $this->accommodationBefore = $accommodationBefore;
    }

    /**
     * @return int
     */
    public function getAccommodationAfter()
    {
        return $this->accommodationAfter;
    }

    /**
     * @param int $accommodationAfter
     */
    public function setAccommodationAfter($accommodationAfter)
    {
        $this->accommodationAfter = $accommodationAfter;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return int
     */
    public function getAccommodationRange()
    {
        return (int)$this->accommodationRange;
    }

    /**
     * @param int $accommodationRange
     */
    public function setAccommodationRange($accommodationRange)
    {
        $this->accommodationRange = $accommodationRange;
    }

    /**
     * @return int
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param int $course
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }

    /**
     * @return int
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param int $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return bool
     */
    public function isArrivalOneDayBefore()
    {
        return $this->arrivalOneDayBefore;
    }

    /**
     * @param bool $arrivalOneDayBefore
     */
    public function setArrivalOneDayBefore($arrivalOneDayBefore)
    {
        $this->arrivalOneDayBefore = $arrivalOneDayBefore;
    }

    /**
     * @return bool
     */
    public function isDepartureOneDayAfter()
    {
        return $this->departureOneDayAfter;
    }

    /**
     * @param bool $departureOneDayAfter
     */
    public function setDepartureOneDayAfter($departureOneDayAfter)
    {
        $this->departureOneDayAfter = $departureOneDayAfter;
    }



}