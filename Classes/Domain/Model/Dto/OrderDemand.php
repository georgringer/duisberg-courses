<?php
namespace GeorgRinger\Courses\Domain\Model\Dto;

use GeorgRinger\Courses\Domain\Model\Order;
use GeorgRinger\News\Domain\Model\DemandInterface;

class OrderDemand extends Order implements DemandInterface
{
    /**
     * @var string
     */
    protected $hash;

    /**
     * @var bool
     * @validate Boolean(is=true)
     */
    protected $privacy = false;

    /**
     * @var string
     */
    protected $birthday;

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return string
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param string $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    public function getRange() {
        return $this->getRanges();
    }

    /**
     * @return bool
     */
    public function isPrivacy()
    {
        return $this->privacy;
    }

    /**
     * @param bool $privacy
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;
    }





}
