<?php
namespace GeorgRinger\Courses\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use DateTime;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * MetaLocation
 */
class MetaLocation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * location
     *
     * @var \GeorgRinger\Courses\Domain\Model\Location
     */
    protected $location = null;

    /**
     * accommodations
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaAccommodation>
     * @cascade remove
     */
    protected $accommodations = null;

    /**
     * foods
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaFood>
     * @cascade remove
     */
    protected $foods = null;

    /**
     * transfers
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaTransfer>
     * @cascade remove
     */
    protected $transfers = null;

    /**
     * rooms
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaRoom>
     * @cascade remove
     */
    protected $rooms = null;

    /** @var string */
    protected $allowedDates = '';

    /** @var string */
    protected $description;

    /** @var int */
    protected $highseasonSurcharge = 0;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->accommodations = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->foods = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->transfers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->rooms = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the location
     *
     * @return \GeorgRinger\Courses\Domain\Model\Location $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location
     *
     * @param \GeorgRinger\Courses\Domain\Model\Location $location
     * @return void
     */
    public function setLocation(\GeorgRinger\Courses\Domain\Model\Location $location)
    {
        $this->location = $location;
    }

    /**
     * Adds a MetaAccommodation
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaAccommodation $accommodation
     * @return void
     */
    public function addAccommodation(\GeorgRinger\Courses\Domain\Model\MetaAccommodation $accommodation)
    {
        $this->accommodations->attach($accommodation);
    }

    /**
     * Removes a MetaAccommodation
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaAccommodation $accommodationToRemove The MetaAccommodation to be removed
     * @return void
     */
    public function removeAccommodation(\GeorgRinger\Courses\Domain\Model\MetaAccommodation $accommodationToRemove)
    {
        $this->accommodations->detach($accommodationToRemove);
    }

    /**
     * Returns the accommodations
     *
     * @return \GeorgRinger\Courses\Domain\Model\MetaAccommodation[]
     */
    public function getAccommodations()
    {
        return $this->accommodations;
    }

    /**
     * Sets the accommodations
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaAccommodation> $accommodations
     * @return void
     */
    public function setAccommodations(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $accommodations)
    {
        $this->accommodations = $accommodations;
    }

    /**
     * Adds a MetaFood
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaFood $food
     * @return void
     */
    public function addFood(\GeorgRinger\Courses\Domain\Model\MetaFood $food)
    {
        $this->foods->attach($food);
    }

    /**
     * Removes a MetaFood
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaFood $foodToRemove The MetaFood to be removed
     * @return void
     */
    public function removeFood(\GeorgRinger\Courses\Domain\Model\MetaFood $foodToRemove)
    {
        $this->foods->detach($foodToRemove);
    }

    /**
     * Returns the foods
     *
     * @return \GeorgRinger\Courses\Domain\Model\MetaFood[]
     */
    public function getFoods()
    {
        return $this->foods;
    }

    /**
     * Sets the foods
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaFood> $foods
     * @return void
     */
    public function setFoods(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $foods)
    {
        $this->foods = $foods;
    }

    /**
     * Adds a MetaTransfer
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaTransfer $transfer
     * @return void
     */
    public function addTransfer(\GeorgRinger\Courses\Domain\Model\MetaTransfer $transfer)
    {
        $this->transfers->attach($transfer);
    }

    /**
     * Removes a MetaTransfer
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaTransfer $transferToRemove The MetaTransfer to be removed
     * @return void
     */
    public function removeTransfer(\GeorgRinger\Courses\Domain\Model\MetaTransfer $transferToRemove)
    {
        $this->transfers->detach($transferToRemove);
    }

    /**
     * Returns the transfers
     *
     * @return \GeorgRinger\Courses\Domain\Model\MetaTransfer[]
     */
    public function getTransfers()
    {
        return $this->transfers;
    }

    /**
     * Sets the transfers
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaTransfer> $transfers
     * @return void
     */
    public function setTransfers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $transfers)
    {
        $this->transfers = $transfers;
    }

    /**
     * Adds a MetaRoom
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaRoom $room
     * @return void
     */
    public function addRoom(\GeorgRinger\Courses\Domain\Model\MetaRoom $room)
    {
        $this->rooms->attach($room);
    }

    /**
     * Removes a MetaRoom
     *
     * @param \GeorgRinger\Courses\Domain\Model\MetaRoom $roomToRemove The MetaRoom to be removed
     * @return void
     */
    public function removeRoom(\GeorgRinger\Courses\Domain\Model\MetaRoom $roomToRemove)
    {
        $this->rooms->detach($roomToRemove);
    }

    /**
     * Returns the rooms
     *
     * @return \GeorgRinger\Courses\Domain\Model\MetaRoom[]
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Sets the rooms
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\Courses\Domain\Model\MetaRoom> $rooms
     * @return void
     */
    public function setRooms(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $rooms)
    {
        $this->rooms = $rooms;
    }

    /**
     * @return string
     */
    public function getAllowedDates()
    {
        return $this->allowedDates;
    }

    /**
     * @return array
     */
    public function getAllowedDatesInFuture()
    {
        $allowedDates = [];
        $allDates = GeneralUtility::trimExplode(LF, $this->getAllowedDates(), true);
        $now = new DateTime();
        foreach ($allDates as $date) {
            $d = DateTime::createFromFormat('d.m.Y', $date);
            if ($d && $d > $now) {
                $allowedDates[$date] = $date;
            }
        }

        return $allowedDates;
    }

    /**
     * @param string $allowedDates
     */
    public function setAllowedDates($allowedDates)
    {
        $this->allowedDates = $allowedDates;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getHighseasonSurcharge()
    {
        return (int)$this->highseasonSurcharge;
    }

    /**
     * @param int $highseasonSurcharge
     */
    public function setHighseasonSurcharge($highseasonSurcharge)
    {
        $this->highseasonSurcharge = $highseasonSurcharge;
    }


}
