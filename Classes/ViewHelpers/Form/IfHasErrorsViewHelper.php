<?php
namespace GeorgRinger\Courses\ViewHelpers\Form;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "TYPO3.Fluid".           *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

class IfHasErrorsViewHelper extends AbstractConditionViewHelper
{
    /**
     * Renders <f:then> child if there are validation errors. The check can be narrowed down to
     * specific property paths.
     * If no errors are there, it renders the <f:else>-child.
     *
     * @param string $for The argument or property name or path to check for error(s)
     * @return string
     * @api
     */
    public function render($for = NULL)
    {
        $validationResults = $this->controllerContext->getRequest()->getOriginalRequestMappingResults();
        if ($validationResults !== null && $for !== '') {
            $validationResults = $validationResults->forProperty($for);
            if (count($validationResults->getErrors()) > 0) {
                return $this->renderThenChild();
            }
        }

        return $this->renderElseChild();
    }
}