<?php
namespace GeorgRinger\Courses\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

class InListViewHelper extends AbstractConditionViewHelper
{
    public function initializeArguments()
    {
        $this->registerArgument('list', 'string', 'List', true);
        $this->registerArgument('item', 'string', 'List', true);
        parent::initializeArguments();
    }

    protected static function evaluateCondition($arguments = null)
    {
        return GeneralUtility::inList($arguments['list'], $arguments['item']);
    }
}
