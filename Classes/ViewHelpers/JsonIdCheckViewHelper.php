<?php

namespace GeorgRinger\Courses\ViewHelpers;

/**
 * This file is part of the "news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;
use TYPO3\CMS\Fluid\Core\ViewHelper\Facets\CompilableInterface;

/**
 * ViewHelper to compare id in json with the one of given
 *
 * # Example: Basic example
 * <code>
 * <c:jsonIdCheck json="{order.course}" id="123">
 *  <f:then>fo</f:then>
 *  <f:else>bar</f:else>
 * </c:jsonIdCheck
 * </code>
 *
 */
class JsonIdCheckViewHelper extends AbstractConditionViewHelper implements CompilableInterface
{
    /**
     */
    public function initializeArguments()
    {
        $this->registerArgument('json', 'string', 'Json', true);
        $this->registerArgument('id', 'int', 'id to compare witz', true);
        parent::initializeArguments();
    }

    /**
     * @param array|null $arguments
     * @return bool
     */
    protected static function evaluateCondition($arguments = null)
    {
        $json = json_decode($arguments['json'], true);
        if (!$json) {
            return false;
        }
        return (int)$json['uid'] === (int)$arguments['id'];
    }
}
