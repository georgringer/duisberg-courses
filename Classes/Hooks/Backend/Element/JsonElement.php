<?php

namespace GeorgRinger\Courses\Hooks\Backend\Element;

use TYPO3\CMS\Backend\Form\AbstractNode;
use TYPO3\CMS\Backend\Form\NodeFactory;
use TYPO3\CMS\Backend\Form\NodeInterface;
use TYPO3\CMS\Core\Utility\DebugUtility;

class JsonElement extends AbstractNode implements NodeInterface
{

    protected $data;

    public function __construct(NodeFactory $nodeFactory, array $data)
    {
        $this->data = $data;
    }

    public function render()
    {
        $table = $this->data['tableName'];
        $fieldName = $this->data['fieldName'];
        $row = $this->data['databaseRow'];

        $parameterArray = $this->data['parameterArray'];
        $resultArray = $this->initializeResultArray();

        $json = $parameterArray['itemFormElValue'];
        if (!empty($json)) {
            $data = json_decode($json, true);

            $resultArray['html'] = DebugUtility::viewArray($data);
        }

        return $resultArray;
    }
}