<?php

namespace GeorgRinger\Courses\Controller;

use GeorgRinger\Courses\Domain\Model\Dto\OrderDemand;
use GeorgRinger\Courses\Domain\Model\MetaLocation;
use GeorgRinger\Courses\Domain\Model\Order;
use GeorgRinger\Courses\Domain\Model\Range;
use GeorgRinger\Courses\Service\MailService;
use GeorgRinger\Courses\Service\PriceService;
use GeorgRinger\Courses\Service\RequestValidator;
use GeorgRinger\News\Domain\Model\DemandInterface;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * OrderController
 */
class BaseContactController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{


    protected function debugMailContent($content, $debug = false)
    {
        if ((bool)$debug) {
            /** @noinspection ForgottenDebugOutputInspection */
            print_r($content);
            die;
        }
    }

    protected function addRelationsToOrder(DemandInterface $order, &$objectInformation = null)
    {
        // initialization
        $course = $food = $accommodation = $transfer = $exam = $range = null;

        $hash = explode('|', $order->getHash());
        $parts = explode('_', $hash[0]);
        $rangeObj = null;
        foreach ($parts as $part) {
            $split = explode('-', $part);
            switch ($split[0]) {
                case 'course':
                    $repositoryName = 'CourseRepository';
                    $isMm = false;
                    break;
                case 'range':
                    $repositoryName = 'RangeRepository';
                    $isMm = false;
                    break;
                case 'exam':
                    $repositoryName = 'MetaexamRepository';
                    $isMm = true;
                    break;
                case 'location':
                    $repositoryName = 'MetalocationRepository';
                    $isMm = true;
                    break;
                case 'transfer':
                    $repositoryName = 'MetatransferRepository';
                    $isMm = true;
                    break;
                case 'room':
                    $repositoryName = 'MetaroomRepository';
                    $isMm = true;
                    break;
                case 'food':
                    $repositoryName = 'MetafoodRepository';
                    $isMm = true;
                    break;
                case 'accommodation':
                    $repositoryName = 'MetaaccommodationRepository';
                    $isMm = true;
                    break;
                default:
                    $isMm = false;
                    $repositoryName = null;
            }
            $id = (int)$split[1];
            if ($repositoryName && $id > 0) {
                $repository = $this->objectManager->get('GeorgRinger\\Courses\\Domain\\Repository\\' . $repositoryName);
                $result = $repository->findByIdentifier($id);
                $setter = 'set' . ucfirst($split[0]);

                $finalName = $split[0];
                $$finalName = $result;

                if ($result) {
                    if ($isMm) {
                        $originalField = 'get' . ucfirst($split[0]);
                        $data = [
                            'uid' => $result->getUid(),
                            'title' => $result->$originalField()->getTitle(),
                        ];
                    } else {
                        $data = ['uid' => $result->getUid(), 'title' => $result->getTitle()];
                    }

                    if (is_array($objectInformation)) {
                        $objectInformation[$split[0]] = $result;
                    } else {
                        $order->$setter(json_encode($data));
                    }
                } else {
                    // @todo what to do
                    echo $repositoryName . '->' . $split[1];
                    die('hmm');
                }
            }
        }

        /** @var MetaLocation $location */
        $highSeasonSurcharge = $location->getHighseasonSurcharge();

        $this->priceService
            ->setAccommodation($accommodation)
            ->setFood($food)
            ->setTransfer($transfer)
            ->setTransferMode($order->getTransferMode())
            ->setAccommodationRange($order->getAccommodationRange())
            ->setExam($exam)
            ->setRange($range)
            ->setDiscount($course->getDiscount())
            ->setHighSeasonSurcharge($highSeasonSurcharge)
            ->setHighSeasonWeeks($this->settings['highSeasonWeeks'])
            ->setArrivalOneDayBefore($order->getArrivalOneDayBefore())
            ->setDepartureOneDayAfter($order->getDepartureOneDayAfter());

        if ($order instanceof OrderDemand) {
            $this->priceService
                ->setCourseTime($order->getStartDate());
        }


        $priceTotal = $this->priceService->getTotalPrice();
        $priceInformation = $this->priceService->getPriceInformation();
        if (is_array($objectInformation)) {
            $objectInformation['price'] = $priceTotal;
            $objectInformation['priceInformation'] = $priceInformation;
        }
        $order->setPrice($priceTotal);
    }

    protected function addErrorFlashMessage()
    {
    }

    /**
     * @param string $key
     * @return string
     */
    protected function translate($key)
    {
        $value = LocalizationUtility::translate($key, 'courses');
        return $value ?: $key;
    }

    public function initializeAction()
    {
        $this->mailService = GeneralUtility::makeInstance(MailService::class);
        $this->priceService = GeneralUtility::makeInstance(PriceService::class);

//        $this->arguments['order']
//            ->getPropertyMappingConfiguration()
//            ->forProperty('startDate')// this line can be skipped in order to specify the format for all properties
//            ->setTypeConverterOption(DateTimeConverter::class, DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'dd/mm/Y');
//
    }


    /** @var MailService */
    protected $mailService;
    /** @var  PriceService */
    protected $priceService;

    /**
     * @return TypoScriptFrontendController
     */
    protected function getTsfe()
    {
        return $GLOBALS['TSFE'];
    }
}
