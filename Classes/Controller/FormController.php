<?php
namespace GeorgRinger\Courses\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use GeorgRinger\Courses\Domain\Model\Dto\SearchDemand;
use GeorgRinger\Courses\Domain\Model\Range;
use GeorgRinger\Courses\Service\PriceService;
use GeorgRinger\Courses\Service\RequestValidator;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * CourseController
 */
class FormController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {

        $result = $this->courseRepository->generateJson(false);
        $json = $result['json'];

        $this->addData($json);

        $js = '<script type="text/javascript">var courses = ' . json_encode($json) . ';</script>';

        $this->view->assignMultiple([
            'facets' => $result['facetList'],
            'courseTitles' => $result['courseTitles'],
            'js' => $js,
        ]);
    }

    /**
     * @param \GeorgRinger\Courses\Domain\Model\Course $course
     * @param \GeorgRinger\Courses\Domain\Model\MetaLocation $location
     * @param SearchDemand $demand
     */
    public function showAction(
        \GeorgRinger\Courses\Domain\Model\Course $course = null,
        \GeorgRinger\Courses\Domain\Model\MetaLocation $location = null,
        \GeorgRinger\Courses\Domain\Model\Dto\SearchDemand $demand = null
    )
    {
        if (is_null($demand)) {
            $demand = $this->objectManager->get(SearchDemand::class);
            if ($location) {
                $demand->setLocation($location->getUid());
            }
            if ($course) {
                $demand->setCourse($course->getUid());

            }
        } else {
            $course = $this->courseRepository->findByIdentifier($demand->getCourse());
            $location = $this->metaLocationRepository->findByIdentifier($demand->getLocation());
        }

        if (is_null($course) || is_null($location)) {
            // todo make nice
            die('Course + Location must be selected, error handling not yet done');
        }

        $accommodation = $food = $transfer = $exam = $range = null;
        $objects = ['food', 'accommodation', 'transfer', 'exam', 'range'];
        foreach ($objects as $name) {
            $getterName = 'get' . ucfirst($name);
            $repositoryName = $name . 'Repository';
            if ($demand->$getterName()) {
                if ($object = $this->$repositoryName->findByIdentifier($demand->$getterName())) {
                    $$name = $object;
                }
            }
        }

        $accommodationRanges = $this->getAccommmodationRanges($course, $range);

        // always fill accommodation range
        if ($demand->getAccommodationRange() === 0 && !is_null($range)) {
            $lastRange = end($accommodationRanges);
            $demand->setAccommodationRange((int)$lastRange);
            reset($accommodationRanges);
        }

        $requestValidation = GeneralUtility::makeInstance(RequestValidator::class);

        // surcharge only if enabled
        $highSeasonSurcharge = $location->getHighseasonSurcharge();

        $this->priceService
            ->setAccommodation($accommodation)
            ->setFood($food)
            ->setTransfer($transfer)
            ->setTransferMode($demand->getTransferMode())
            ->setAccommodationRange($demand->getAccommodationRange())
            ->setExam($exam)
            ->setRange($range)
            ->setHighSeasonSurcharge($highSeasonSurcharge)
            ->setHighSeasonWeeks($this->settings['highSeasonWeeks'])
            ->setDiscount($course->getDiscount())
            ->setArrivalOneDayBefore($demand->isArrivalOneDayBefore())
            ->setDepartureOneDayAfter($demand->isDepartureOneDayAfter())
            ->setCourseTime($demand->getStartDate());

        $totalPrice = $this->priceService->getTotalPrice();
        $priceInformation = $this->priceService->getPriceInformation();
        $this->view->assignMultiple([
            'totalPrice' => $totalPrice,
            'priceInformation' => $priceInformation,
            'course' => $course,
            'location' => $location,
            'accommodation' => $accommodation,
            'accommodationRanges' => $accommodationRanges,
            'food' => $food,
            'transfer' => $transfer,
            'exam' => $exam,
            'range' => $range,
            'demand' => $demand,
            'hash' => $requestValidation->generateHash($course, $location, $accommodation, $food, $transfer,
                $exam, $range)
        ]);
    }


    protected function addData(array &$json)
    {
        $facetFields = ['accommodation', 'food', 'room'];
        foreach ($json as &$item) {
            // price
            $item['priceTotal'] = 0;
            foreach ($facetFields as $field) {
                $item['priceTotal'] += (int)$item[$field . '.price'];
            }

            // links
            $linkArguments = [
                'course' => $item['uid'],
                'location' => $item['location.uid'],
            ];
            $item['link'] = $this->generateLink($linkArguments);
            foreach ($facetFields as $field) {
                $linkArguments[$field] = $item[$field . '.uid'];
            }
            $item['linkFull'] = $this->generateLink($linkArguments);
        }
    }

    protected function getAccommmodationRanges(\GeorgRinger\Courses\Domain\Model\Course $course, \GeorgRinger\Courses\Domain\Model\Range $selectedRange = null) {
        $maxRange = $selectedRange ? (int)$selectedRange->getValue() : 999;
        $ranges = [];
        if (is_null($selectedRange)) {
            return $ranges;
        } elseif($selectedRange->getMode() === 2 || $selectedRange->getMode() === 3) {
            $availableRanges = $this->rangeRepository->findRangeByPidAndMode($this->settings['pidForAlternativeRangeTypes'], $selectedRange->getMode());
            foreach ($availableRanges as $range) {
                /** @var Range $range */
//                if ((int)$range->getValue() <= $maxRange) {
                    $ranges[(int)$range->getValue()] = $range->getValue();
//                }
            }
        } else {
            foreach ($course->getRanges() as $range) {
                /** @var Range $range */
                if ((int)$range->getValue() <= $maxRange && $selectedRange->getMode() == $range->getMode()) {
                    $ranges[(int)$range->getValue()] = $range->getValue();
                }
            }
        }
        return $ranges;
    }

    protected function generateLink(
        $arguments,
        $actionName = 'show',
        $pageUid = null
    )
    {
        $controllerName = $this->request->getControllerName();
        $this->uriBuilder->reset()->setTargetPageUid($pageUid)->setCreateAbsoluteUri(true);
        if (GeneralUtility::getIndpEnv('TYPO3_SSL')) {
            $this->uriBuilder->setAbsoluteUriScheme('https');
        }
        $uri = $this->uriBuilder->uriFor($actionName, $arguments, $controllerName);
        return $uri;
    }


    /**
     * @var \GeorgRinger\Courses\Domain\Repository\CourseRepository
     * @inject
     */
    protected $courseRepository = null;

    /**
     * @var \GeorgRinger\Courses\Domain\Repository\MetalocationRepository
     * @inject
     */
    protected $metaLocationRepository = null;

    /**
     * @var \GeorgRinger\Courses\Domain\Repository\MetafoodRepository
     * @inject
     */
    protected $foodRepository = null;

    /**
     * @var \GeorgRinger\Courses\Domain\Repository\MetaaccommodationRepository
     * @inject
     */
    protected $accommodationRepository = null;

    /**
     * @var \GeorgRinger\Courses\Domain\Repository\MetatransferRepository
     * @inject
     */
    protected $transferRepository = null;

    /**
     * @var \GeorgRinger\Courses\Domain\Repository\MetaexamRepository
     * @inject
     */
    protected $examRepository = null;

    /**
     * @var \GeorgRinger\Courses\Domain\Repository\RangeRepository
     * @inject
     */
    protected $rangeRepository = null;

    public function initializeAction()
    {
        $this->priceService = GeneralUtility::makeInstance(PriceService::class);
    }

    /** @var PriceService */
    protected $priceService;
}
