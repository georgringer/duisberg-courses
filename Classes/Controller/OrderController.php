<?php

namespace GeorgRinger\Courses\Controller;

use GeorgRinger\Courses\Domain\Model\Dto\OrderDemand;
use GeorgRinger\Courses\Domain\Model\MetaLocation;
use GeorgRinger\Courses\Domain\Model\Order;
use GeorgRinger\Courses\Domain\Model\Range;
use GeorgRinger\Courses\Service\MailService;
use GeorgRinger\Courses\Service\PriceService;
use GeorgRinger\Courses\Service\RequestValidator;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * OrderController
 */
class OrderController extends BaseContactController
{

    /**
     * @param OrderDemand $order
     * @param string $hash
     * @param array $extras
     */
    public function newAction(OrderDemand $order = null, $hash = '', $extras = [])
    {
        $isNew = false;
        if (is_null($order)) {
            $order = $this->objectManager->get(OrderDemand::class);
            $isNew = true;
        }
        $extraFields = ['transferMode', 'accommodationRange', 'startDate', 'arrivalOneDayBefore', 'departureOneDayAfter'];
        foreach ($extraFields as $field) {
            if (isset($extras[$field])) {
                $setter = 'set' . ucfirst($field);
                $order->$setter($extras[$field]);
            }
        }

        $orderInformation = [];
        $order->setHash($hash);
        $this->addRelationsToOrder($order, $orderInformation);

        // prefill fields (see #638)
        if ($isNew) {
            $startDate = $order->getStartDate();
            if ($startDate) {
                $startDateAsDate = \DateTime::createFromFormat('d.m.Y', $startDate);


                $order->setInsuranceStart($startDate);
                $order->setDateArrival($startDate);

                if ($startDateAsDate) {
                    $beginMinusOne = clone $startDateAsDate;

                    $endDate = clone $startDateAsDate;

                    if ($orderInformation['accommodation']) {
                        // one day off is must anyway
                        $beginMinusOne->modify('-1 day');
                        $endDate->modify('previous saturday');

                        // additional extra day before
                        if ($order->getArrivalOneDayBefore()) {
                            $beginMinusOne->modify('-1 day');
                        }

                        // additional extra day after
                        if ($order->getDepartureOneDayAfter()) {
                            $endDate->modify('+1 day');
                        }

                        $accommodationRange = $order->getAccommodationRange();
                        if ($accommodationRange) {
                            $endDate->modify('+' . $accommodationRange . ' weeks');
                            $order->setDateDeparture($endDate->format('d.m.Y'));
                            $order->setInsuranceEnd($endDate->format('d.m.Y'));
                        }
                    } else {
                        $endDate->modify('previous friday');
                        /** @var Range $range */
                        $range = $orderInformation['range'];
                        if ($range) {
                            $endDate = $endDate->modify('+' . (int)$range->getValue() . ' weeks');
                            $order->setDateDeparture($endDate->format('d.m.Y'));
                            $order->setInsuranceEnd($endDate->format('d.m.Y'));
                        }
                    }

                    $order->setInsuranceStart($beginMinusOne->format('d.m.Y'));
                    $order->setDateArrival($beginMinusOne->format('d.m.Y'));
                }
            }
        }


        $this->view->assignMultiple([
            'extras' => $extras,
            'order' => $order,
            'orderInformation' => $orderInformation
        ]);
    }

    /**
     * @param OrderDemand $order
     */
    public function validateAction(OrderDemand $order = null)
    {
        if (is_null($order)) {
            $this->redirect('new');
        }

        $orderInformation = [];
        $this->addRelationsToOrder($order, $orderInformation);

        $this->view->assignMultiple([
            'order' => $order,
            'orderInformation' => $orderInformation
        ]);
    }


    /**
     * @param OrderDemand $order
     */
    public function createAction(OrderDemand $order)
    {
        /** @var RequestValidator $requestValidator */
        $requestValidator = GeneralUtility::makeInstance(RequestValidator::class);
        $validHash = $requestValidator->validateHash($order->getHash());
        if (!$validHash) {
            // @todo log?
            $this->redirect('list', 'Course');
        }

        $this->addRelationsToOrder($order);

        $orderInformation = [];
        $this->addRelationsToOrder($order, $orderInformation);

        $this->sendMailToUser($order, $orderInformation);
        $this->sendMailToAdmin($order, $orderInformation);

        if ($this->settings['newOrder']['pid']) {
            $order->setPid($this->settings['newOrder']['pid']);
        }

        $finalOrder = $this->createOrder($order);
        if ($finalOrder) {
            $orderHash = GeneralUtility::hmac($finalOrder->getUid(), 'course');

            $this->redirect(
                'success',
                null,
                null,
                [
                    'order' => $finalOrder->getUid(),
                    'orderHash' => $orderHash
                ]
            );
        } else {
            $this->redirect('success');
        }
    }

    /**
     * @param OrderDemand $orderDemand
     * @return Order
     */
    protected function createOrder(OrderDemand $orderDemand)
    {
        /** @var Order $order */
        $order = $this->objectManager->get(Order::class);
        $methods = get_class_methods(Order::class);
        foreach ($methods as $method) {
            try {
                if (StringUtility::beginsWith($method, 'set')) {
                    $getter = 'g' . substr($method, 1);
                    $value = $orderDemand->$getter();
                    switch ($method) {
                        case 'setBirthday':
                            $value = new \DateTime($value);
                            break;
                    }
                    $order->$method($value);
                }
            } catch (\Exception $e) {
                // do nothing
            }
        }

//        print_r($order);die;
        $this->orderRepository->add($order);
        $this->objectManager->get(PersistenceManager::class)->persistAll();

        return $order;
    }

    public function successAction()
    {
        $vars = GeneralUtility::_GET('tx_courses_course');

        if (isset($vars['order']) && isset($vars['orderHash'])) {

            // check hash
            $calculcatedHash = GeneralUtility::hmac($vars['order'], 'course');
            if ($calculcatedHash === $vars['orderHash']) {
                $this->view->assignMultiple([
                    'order' => $this->orderRepository->findByIdentifier((int)$vars['order'])
                ]);
            }
        }


    }

    protected function sendMailToUser(OrderDemand $orderDemand, array $orderInformation)
    {
        $arguments = [
            'orderInformation' => $orderInformation,
            'order' => $orderDemand,
            'language' => $this->getTsfe()->sys_language_uid
        ];
        $settings = $this->settings['mailToUser'];

        if ($settings['enable'] == 1) {
            $plainContent = $this->mailService->getFluidTemplate($arguments, 'MailToUser.txt', $settings['templatePath']);
            $htmlContent = $this->mailService->getFluidTemplate($arguments, 'MailToUser.html', $settings['templatePath'], 'html');
            $this->debugMailContent($plainContent, $settings['debug']);
            /** @var MailMessage $mailMessage */
            $mailMessage = GeneralUtility::makeInstance(MailMessage::class);
            $mailMessage
                ->setSubject($this->translate($settings['subject']))
                ->addFrom($settings['fromMailAddress'], $settings['fromMailName'])
                ->setTo($orderDemand->getEmail());
            $this->mailService->sendMail($mailMessage, $plainContent);
        }
    }

    protected function sendMailToAdmin(OrderDemand $order, array $orderInformation)
    {
        $arguments = [
            'orderInformation' => $orderInformation,
            'order' => $order,
            'language' => $this->getTsfe()->sys_language_uid,
        ];
        $settings = $this->settings['mailToAdmin'];
        if ($settings['enable'] == 1) {
            $plainContent = $this->mailService->getFluidTemplate($arguments, 'MailToAdmin.txt', $settings['templatePath']);
            $htmlContent = $this->mailService->getFluidTemplate($arguments, 'MailToAdmin.html', $settings['templatePath'], 'html');
            $this->debugMailContent($plainContent, $settings['debug']);

            $to = $settings['centerMapping']['default'];
            $location = $order->getLocationEncoded();
            if (!empty($location['title'])) {
                $title = str_replace(['ö', 'ä', 'ü'], ['oe', 'ae', 'ue'], trim($location['title']));
                if (isset($settings['centerMapping'][$title])) {
                    $to = $settings['centerMapping'][$title];
                }
            }

            $subject = $this->translate($settings['subject']);
            $subject = $this->mailService->getInlineFluidTemplate($arguments, $subject);

            /** @var MailMessage $mailMessage */
            $mailMessage = GeneralUtility::makeInstance(MailMessage::class);
            $mailMessage
                ->setSubject($subject)
                ->addFrom($settings['fromMailAddress'], $settings['fromMailName'])
                ->setTo($to);
            if (isset($settings['cc'])) {
                $mailMessage->setCc($settings['cc']);
            }
            $this->mailService->sendMail($mailMessage, $plainContent, $htmlContent);
        }
    }

    /**
     * orderRepository
     *
     * @var \GeorgRinger\Courses\Domain\Repository\OrderRepository
     * @inject
     */
    protected $orderRepository = null;


}
