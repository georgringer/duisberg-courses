<?php

namespace GeorgRinger\Courses\Controller;

use GeorgRinger\Courses\Domain\Model\Dto\RequestDemand;
use GeorgRinger\Courses\Domain\Model\Request;
use GeorgRinger\Courses\Service\RequestValidator;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/***
 *
 * This file is part of the "Course Configuration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * RequestController
 */
class RequestController extends BaseContactController
{
    /**
     * requestRepository
     *
     * @var \GeorgRinger\Courses\Domain\Repository\RequestRepository
     * @inject
     */
    protected $requestRepository = null;

    /**
     * @param RequestDemand $order
     * @param string $hash
     * @param array $extras
     */
    public function newAction(RequestDemand $request = null, $hash = '', $extras = [])
    {
        $isNew = false;
        if (is_null($request)) {
            $request = $this->objectManager->get(RequestDemand::class);
            $isNew = true;
        }
        $extraFields = ['transferMode', 'accommodationRange', 'arrivalOneDayBefore', 'departureOneDayAfter'];
        foreach ($extraFields as $field) {
            if (isset($extras[$field])) {
                $setter = 'set' . ucfirst($field);
                $request->$setter($extras[$field]);
            }
        }

        $orderInformation = [];
        $request->setHash($hash);
        $this->addRelationsToOrder($request, $orderInformation);

        // pref

        $this->view->assignMultiple([
            'extras' => $extras,
            'request' => $request,
            'orderInformation' => $orderInformation
        ]);
    }


    /**
     * @param RequestDemand $request
     */
    public function validateAction(RequestDemand $request = null)
    {
        if (is_null($request)) {
            $this->redirect('new');
        }

        $orderInformation = [];
        $this->addRelationsToOrder($request, $orderInformation);

        $this->view->assignMultiple([
            'request' => $request,
            'orderInformation' => $orderInformation
        ]);
    }


    /**
     * @param RequestDemand $request
     */
    public function createAction(RequestDemand $request)
    {
        /** @var RequestValidator $requestValidator */
        $requestValidator = GeneralUtility::makeInstance(RequestValidator::class);
        $validHash = $requestValidator->validateHash($request->getHash());
        if (!$validHash) {
            // @todo log?
            $this->redirect('list', 'Course');
        }

        $this->addRelationsToOrder($request);

        $orderInformation = [];
        $this->addRelationsToOrder($request, $orderInformation);

        $this->sendMailToUser($request, $orderInformation);
        $this->sendMailToAdmin($request, $orderInformation);

        if ($this->settings['newRequest']['pid']) {
            $request->setPid($this->settings['newRequest']['pid']);
        }

        $finalRequest = $this->createRequest($request);
        if ($finalRequest) {
            $requestHash = GeneralUtility::hmac($finalRequest->getUid(), 'course');

            $this->redirect(
                'success',
                null,
                null,
                [
                    'request' => $finalRequest->getUid(),
                    'requestHash' => $requestHash
                ]
            );
        } else {
            $this->redirect('success');
        }
    }

    /**
     * @param RequestDemand $requestDemand
     * @return Request
     */
    protected function createRequest(RequestDemand $requestDemand)
    {
        /** @var Order $request */
        $request = $this->objectManager->get(Request::class);
        $methods = get_class_methods(Request::class);
        foreach ($methods as $method) {
            try {
                if (StringUtility::beginsWith($method, 'set')) {
                    $getter = 'g' . substr($method, 1);
                    $value = $requestDemand->$getter();
//                    switch ($method) {
//                        case 'setBirthday':
//                            $value = new \DateTime($value);
//                            break;
//                    }
                    $request->$method($value);
                }
            } catch (\Exception $e) {
                // do nothing
            }
        }

//        print_r($request);die;
        $this->requestRepository->add($request);
        $this->objectManager->get(PersistenceManager::class)->persistAll();

        return $request;
    }

    public function successAction()
    {
        $vars = GeneralUtility::_GET('tx_courses_course');

        if (isset($vars['request']) && isset($vars['requestHash'])) {

            // check hash
            $calculcatedHash = GeneralUtility::hmac($vars['request'], 'course');
            if ($calculcatedHash === $vars['requestHash']) {
                $this->view->assignMultiple([
                    'request' => $this->requestRepository->findByIdentifier((int)$vars['request'])
                ]);
            }
        }
    }


    protected function sendMailToUser(RequestDemand $requestDemand, array $orderInformation)
    {
        $arguments = [
            'orderInformation' => $orderInformation,
            'request' => $requestDemand,
            'language' => $this->getTsfe()->sys_language_uid
        ];
        $settings = $this->settings['requestMailToUser'];

        if ($settings['enable'] == 1) {
            $plainContent = $this->mailService->getFluidTemplate($arguments, 'RequestMailToUser.txt', $settings['templatePath']);
            $htmlContent = $this->mailService->getFluidTemplate($arguments, 'RequestMailToUser.html', $settings['templatePath'], 'html');
            $this->debugMailContent($plainContent, $settings['debug']);
            /** @var MailMessage $mailMessage */
            $mailMessage = GeneralUtility::makeInstance(MailMessage::class);
            $mailMessage
                ->setSubject($this->translate($settings['subject']))
                ->addFrom($settings['fromMailAddress'], $settings['fromMailName'])
                ->setTo($requestDemand->getEmail());
            $this->mailService->sendMail($mailMessage, $plainContent);
        }
    }

    protected function sendMailToAdmin(RequestDemand $requestDemand, array $orderInformation)
    {
        $arguments = [
            'orderInformation' => $orderInformation,
            'request' => $requestDemand,
            'language' => $this->getTsfe()->sys_language_uid,
        ];
        $settings = $this->settings['requestMailToAdmin'];
        if ($settings['enable'] == 1) {
            $plainContent = $this->mailService->getFluidTemplate($arguments, 'RequestMailToAdmin.txt', $settings['templatePath']);
            $htmlContent = $this->mailService->getFluidTemplate($arguments, 'RequestMailToAdmin.html', $settings['templatePath'], 'html');
            $this->debugMailContent($plainContent, $settings['debug']);


            $subject = $this->translate($settings['subject']);
            $subject = $this->mailService->getInlineFluidTemplate($arguments, $subject);

            /** @var MailMessage $mailMessage */
            $mailMessage = GeneralUtility::makeInstance(MailMessage::class);
            $mailMessage
                ->setSubject($subject)
                ->addFrom($settings['fromMailAddress'], $settings['fromMailName'])
                ->setTo($settings['to']);
            if (isset($settings['cc'])) {
                $mailMessage->setCc($settings['cc']);
            }
            $this->mailService->sendMail($mailMessage, $plainContent, $htmlContent);
        }
    }
}
