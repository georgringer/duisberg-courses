
plugin.tx_courses_course {
  view {
    # cat=plugin.tx_courses_course/file; type=string; label=Path to template root (FE)
    templateRootPath =
    # cat=plugin.tx_courses_course/file; type=string; label=Path to template partials (FE)
    partialRootPath =
    # cat=plugin.tx_courses_course/file; type=string; label=Path to template layouts (FE)
    layoutRootPath =
  }
  persistence {
    # cat=plugin.tx_courses_course//a; type=string; label=Default storage PID
    storagePid =
  }
}
