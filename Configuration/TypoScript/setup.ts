
plugin.tx_courses_course {
  view {
    templateRootPaths.0 = EXT:courses/Resources/Private/Templates/
    templateRootPaths.1 = {$plugin.tx_courses_course.view.templateRootPath}
    partialRootPaths.0 = EXT:courses/Resources/Private/Partials/
    partialRootPaths.1 = {$plugin.tx_courses_course.view.partialRootPath}
    layoutRootPaths.0 = EXT:courses/Resources/Private/Layouts/
    layoutRootPaths.1 = {$plugin.tx_courses_course.view.layoutRootPath}
  }
  persistence {
    storagePid = {$plugin.tx_courses_course.persistence.storagePid}
  }
}

plugin.tx_courses._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-courses table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-courses table th {
        font-weight:bold;
    }

    .tx-courses table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)
