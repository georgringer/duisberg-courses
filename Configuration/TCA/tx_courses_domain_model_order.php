<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order',
        'label' => 'last_name',
        'label_alt' => 'first_name,salutation',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'first_name,last_name,salutation,company,street,zip,city,country,nationality,birthday,passport_number,telephone,email,visa_required,learned_german,german_level,previous_course,previous_course_location,previous_course_year,allergies,smoker,comment,correspondence_language,known_from,payment_type,course,location,accommodation,food,room,transfer,exam,ranges,price,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('courses') . 'Resources/Public/Icons/tx_courses_domain_model_order.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, first_name, last_name, salutation, company, work_for_companystreet, zip, city, country, nationality, birthday, passport_number, telephone, email, visa_required, learned_german, german_level, previous_course, previous_course_location, previous_course_year, allergies, smoker, comment, correspondence_language, known_from, payment_type, course, location, accommodation, food, room, transfer, exam, ranges, price,agb',
    ],
    'types' => [
        '1' => [
            'showitem' => '
        ,--div--;User,last_name,--palette--;;paletteName, street,--palette--;;paletteAddress, birthday, passport_number, visa_required,  , payment_type
        ,--div--;Extras,allergies, smoker, comment, correspondence_language, known_from
        ,--div--;CourseInfo,learned_german, german_level, previous_course, previous_course_location, previous_course_year,
        ,--div--;Order, price,start_date,course, location, accommodation,--palette--;;paletteAccommodation, food, room, transfer, transfer_mode, exam, ranges,date_arrival,date_departure,
            
            arrival_one_day_before, departure_one_day_after,
            additional_information,agency,insurance,--palette--;;paletteInsurance,agb
        
        '
        ],
    ],
    'palettes' => [
        'paletteName' => [
            'showitem' => 'first_name,salutation,company,work_for_company
            ,--linebreak--,telephone, email'
        ],
        'paletteAddress' => ['showitem' => 'zip, city, country, nationality, '],
        'paletteAccommodation' => ['showitem' => 'accommodation_range'],
        'paletteInsurance' => ['showitem' => 'express_service,insurance_start,insurance_end'],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'first_name' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.first_name',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim'
            ],

        ],
        'last_name' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.last_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'salutation' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.salutation',
            'config' => [
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim'
            ],
        ],
        'company' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.company',
            'config' => [
                'type' => 'input',
                'size' => 15,
                'eval' => 'trim'
            ],

        ],
        'work_for_company' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.work_for_company',
            'config' => [
                'type' => 'check'
            ],

        ],
        'street' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'zip' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.zip',
            'config' => [
                'type' => 'input',
                'size' => 3,
                'eval' => 'trim'
            ],

        ],
        'city' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.city',
            'config' => [
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim'
            ],

        ],
        'country' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.country',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'trim'
            ],

        ],
        'nationality' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.nationality',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'trim'
            ],

        ],
        'birthday' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.birthday',
            'config' => [
                'type' => 'input',
                'size' => 7,
                'eval' => 'date',
                'checkbox' => 1,
                'default' => time()
            ],

        ],
        'passport_number' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.passport_number',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'telephone' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.telephone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'email' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'visa_required' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.visa_required',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]

        ],
        'learned_german' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.learned_german',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]

        ],
        'german_level' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.german_level',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'previous_course' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.previous_course',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]

        ],
        'previous_course_location' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.previous_course_location',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'previous_course_year' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.previous_course_year',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'allergies' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.allergies',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 2,
                'eval' => 'trim'
            ]

        ],
        'smoker' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.smoker',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]

        ],
        'comment' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.comment',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'eval' => 'trim'
            ]

        ],
        'additional_information' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.additional_information',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'eval' => 'trim'
            ]
        ],
        'correspondence_language' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.correspondence_language',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'known_from' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.known_from',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'eval' => 'trim'
            ]

        ],
        'payment_type' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.payment_type',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'course' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.course',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],

        ],
        'location' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.location',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],

        ],
        'accommodation' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.accommodation',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],
        ],
        'accommodation_range' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.accommodation_range',
            'config' => [
                'type' => 'input',
                'size' => 2,
            ],
        ],
        'food' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.food',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],
        ],
        'room' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.room',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],
        ],
        'transfer' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.transfer',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],
        ],
        'transfer_mode' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.transfer_mode',
            'config' => [
                'type' => 'select',
                'size' => 30,
                'items' => [
                    ['', 0],
                    ['Both', 1],
                    ['Hin', 2],
                    ['Retour', 3],
                ]
            ],
        ],
        'exam' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.exam',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],

        ],
        'ranges' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.ranges',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],

        ],
        'price' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.price',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]

        ],
        'start_date' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.start_date',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'date'
            ]
        ],
        'date_departure' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.date_departure',
            'config' => [
                'type' => 'input',
                'size' => 4,
            ]
        ],
        'date_arrival' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.date_arrival',
            'config' => [
                'type' => 'input',
                'size' => 4,
            ]
        ],
        'express_service' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.express_service',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'insurance' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.insurance',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'insurance_start' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.insurance_start',
            'config' => [
                'type' => 'input',
                'size' => 4,
            ]
        ],
        'insurance_end' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.insurance_end',
            'config' => [
                'type' => 'input',
                'size' => 4,
            ]
        ],
        'agb' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.agb',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'arrival_one_day_before' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.arrival_one_day_before',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'departure_one_day_after' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.departure_one_day_after',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'agency' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.agency',
            'config' => [
                'type' => 'input',
            ]
        ],
    ],
];
