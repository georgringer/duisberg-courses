<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'sortby' => 'sorting',

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',

        ],
        'searchFields' => 'title,insurance,courier_service,ranges,locations,exams,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('courses') . 'Resources/Public/Icons/tx_courses_domain_model_course.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, insurance, courier_service, ranges, locations, exams',
    ],
    'types' => [
        '1' => [
            'columnsOverrides' => [
                'description' => [
                    'defaultExtras' => 'richtext:rte_transform[mode=ts_css]'
                ],
            ],
            'showitem' => '
                sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, insurance, courier_service,discount,
                --div--;Kursbeschreibung,media,teaser,description,
                --div--;Unterkünfte,locations,
                --div--;Ranges,ranges,
                --div--;Prüfungen,exams'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0]
                ],
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_courses_domain_model_course',
                'foreign_table_where' => 'AND tx_courses_domain_model_course.pid=###CURRENT_PID### AND tx_courses_domain_model_course.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],

        'title' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'insurance' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course.insurance',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]

        ],
        'courier_service' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course.courier_service',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]
        ],
        'media' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course.media',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'media'
            ),
        ],
        'teaser' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course.teaser',
            'config' => [
                'type' => 'text',
                'cols' => 60,
                'rows' => 4,
                'default' => ''
            ]
        ],
        'description' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course.description',
            'config' => [
                'type' => 'text',
                'cols' => 30,
                'rows' => 4,
                'softref' => 'rtehtmlarea_images,typolink_tag,images,email[subst],url',
            ]
        ],
        'ranges' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course.ranges',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_courses_domain_model_range',
                'foreign_field' => 'course',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
        'locations' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course.locations',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_courses_domain_model_metalocation',
                'foreign_field' => 'course',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
        'exams' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course.exams',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_courses_domain_model_metaexam',
                'foreign_field' => 'course',
                'maxitems' => 9999,
                'foreign_default_sortby' => 'hidden',
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
        'discount' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_course.discount',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'default' => ''
            ]
        ],

    ],
];
