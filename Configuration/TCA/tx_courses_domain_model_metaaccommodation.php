<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metaaccommodation',
        'label' => 'price',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',

        ],
        'searchFields' => 'price,accommodation,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('courses') . 'Resources/Public/Icons/tx_courses_domain_model_metaaccommodation.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, price, accommodation',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, price, day_surcharge,accommodation, '],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0]
                ],
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_courses_domain_model_metaaccommodation',
                'foreign_table_where' => 'AND tx_courses_domain_model_metaaccommodation.pid=###CURRENT_PID### AND tx_courses_domain_model_metaaccommodation.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],

        'price' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metaaccommodation.price',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]

        ],
        'day_surcharge' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metaaccommodation.day_surcharge',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'accommodation' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metaaccommodation.accommodation',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_courses_domain_model_accommodation',
                'minitems' => 0,
                'maxitems' => 1,
            ],

        ],

        'metalocation' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
