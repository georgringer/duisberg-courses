<?php

$tables = [
    'accommodation',
    'course',
    'exam',
    'food',
    'location',
    'range',
    'roomtype',
    'transfer',
    'metaaccommodation',
    'metaexam',
    'metaroom',
    'metafood',
    'metalocation',
    'metatransfer'
];

foreach ($tables as $table) {
    $GLOBALS['TCA']['tx_courses_domain_model_' . $table]['columns']['sys_language_uid']['config'] = [
        'type' => 'select',
        'renderType' => 'selectSingle',
        'special' => 'languages',
        'items' => [
            [
                'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                -1,
                'flags-multiple'
            ],
        ],
        'default' => 0,
    ];
}

$hideTables = [
    'metaaccommodation',
    'metaexam',
    'metafood',
    'metaroom',
    'metalocation',
    'metatransfer'
];

foreach ($hideTables as $table) {
    $GLOBALS['TCA']['tx_courses_domain_model_' . $table]['ctrl']['hideTable'] = true;
}

$GLOBALS['TCA']['tx_courses_domain_model_course']['columns']['locations']['config']['appearance']['collapseAll'] = true;
$GLOBALS['TCA']['tx_courses_domain_model_course']['columns']['locations']['config']['foreign_label'] = 'location';
$GLOBALS['TCA']['tx_courses_domain_model_course']['columns']['locations']['config']['foreign_selector'] = 'location';
$GLOBALS['TCA']['tx_courses_domain_model_course']['columns']['locations']['config']['foreign_unique'] = 'location';

$GLOBALS['TCA']['tx_courses_domain_model_course']['columns']['exams']['config']['appearance']['collapseAll'] = true;
$GLOBALS['TCA']['tx_courses_domain_model_course']['columns']['exams']['config']['foreign_label'] = 'exam';
$GLOBALS['TCA']['tx_courses_domain_model_course']['columns']['exams']['config']['foreign_selector'] = 'exam';
$GLOBALS['TCA']['tx_courses_domain_model_course']['columns']['exams']['config']['foreign_unique'] = 'exam';

$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['accommodations']['config']['appearance']['collapseAll'] = true;
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['accommodations']['config']['appearance']['useSortable'] = true;
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['accommodations']['config']['foreign_label'] = 'accommodation';
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['accommodations']['config']['foreign_selector'] = 'accommodation';
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['accommodations']['config']['foreign_sortby'] = 'sorting';
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['accommodations']['config']['foreign_unique'] = 'accommodation';

$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['foods']['config']['appearance']['collapseAll'] = true;
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['foods']['config']['foreign_label'] = 'food';
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['foods']['config']['foreign_selector'] = 'food';
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['foods']['config']['foreign_unique'] = 'food';

$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['transfers']['config']['appearance']['collapseAll'] = true;
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['transfers']['config']['foreign_label'] = 'transfer';
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['transfers']['config']['foreign_selector'] = 'transfer';
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['transfers']['config']['foreign_unique'] = 'transfer';

$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['rooms']['config']['appearance']['collapseAll'] = true;
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['rooms']['config']['foreign_label'] = 'room';
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['rooms']['config']['foreign_selector'] = 'room';
$GLOBALS['TCA']['tx_courses_domain_model_metalocation']['columns']['rooms']['config']['foreign_unique'] = 'room';


$GLOBALS['TCA']['tx_courses_domain_model_course']['columns']['ranges']['config']['appearance']['collapseAll'] = true;
$GLOBALS['TCA']['tx_courses_domain_model_range']['ctrl']['label'] = 'price';
$GLOBALS['TCA']['tx_courses_domain_model_range']['ctrl']['label_alt'] = 'value,mode';
$GLOBALS['TCA']['tx_courses_domain_model_range']['ctrl']['label_alt_force'] = true;
$GLOBALS['TCA']['tx_courses_domain_model_range']['types']['1']['showitem'] = 'value;;1';
$GLOBALS['TCA']['tx_courses_domain_model_range']['palettes']['1']['showitem'] = 'price,mode';
$GLOBALS['TCA']['tx_courses_domain_model_range']['columns']['mode']['config']['items'] = [
  ['pro Woche', '1'],
  ['TE', '2'],
  ['pauschal', '3'],

];