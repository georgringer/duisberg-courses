<?php
defined('TYPO3_MODE') or die();

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['courses_courselink'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['courses_course'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['courses_courselink'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('courses_courselink',
    'FILE:EXT:courses/Configuration/FlexForms/flexform_courses_courselink.xml');


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'GeorgRinger.courses',
    'Course',
    'Course Configuration'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'GeorgRinger.courses',
    'Courselink',
    'Course Link'
);