<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_request',
        'label' => 'salutation',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',

        'enablecolumns' => [

        ],
        'searchFields' => 'salutation,first_name,last_name,telephone,country,email,',
        'iconfile' => 'EXT:courses/Resources/Public/Icons/tx_courses_domain_model_request.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, salutation, first_name, last_name, telephone, country, email',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, salutation, first_name, last_name, telephone, country, email, '],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_courses_domain_model_request',
                'foreign_table_where' => 'AND tx_courses_domain_model_request.pid=###CURRENT_PID### AND tx_courses_domain_model_request.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        'salutation' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_request.salutation',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'first_name' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_request.first_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'last_name' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_request.last_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'telephone' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_request.telephone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'country' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_request.country',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],
        'email' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_request.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],

        ],

        'course' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.course',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],

        ],
        'location' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.location',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],

        ],
        'accommodation' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.accommodation',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],
        ],
        'accommodation_range' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.accommodation_range',
            'config' => [
                'type' => 'input',
                'size' => 2,
            ],
        ],
        'food' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.food',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],
        ],
        'room' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.room',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],
        ],
        'transfer' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.transfer',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],
        ],
        'transfer_mode' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.transfer_mode',
            'config' => [
                'type' => 'select',
                'size' => 30,
                'items' => [
                    ['', 0],
                    ['Both', 1],
                    ['Hin', 2],
                    ['Retour', 3],
                ]
            ],
        ],
        'exam' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.exam',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],

        ],
        'ranges' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.ranges',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'renderType' => 'json',
            ],

        ],
        'price' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.price',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]

        ],
        'arrival_one_day_before' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.arrival_one_day_before',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'departure_one_day_after' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_order.departure_one_day_after',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],

    ],
];
