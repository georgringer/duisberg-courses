<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metalocation',
        'label' => 'location',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',

        ],
        'searchFields' => 'location,accommodations,foods,transfers,rooms,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('courses') . 'Resources/Public/Icons/tx_courses_domain_model_metalocation.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, location, accommodations, allowed_dates, description, transfers, rooms',
    ],
    'types' => [
        '1' => [
            'showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, location, accommodations, allowed_dates, description,transfers, highseason_surcharge',
            'columnsOverrides' => [
                'description' => [
                    'defaultExtras' => 'richtext:rte_transform[mode=ts_css]'
                ],
            ],
        ],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0]
                ],
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_courses_domain_model_metalocation',
                'foreign_table_where' => 'AND tx_courses_domain_model_metalocation.pid=###CURRENT_PID### AND tx_courses_domain_model_metalocation.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],

        'location' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metalocation.location',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_courses_domain_model_location',
                'minitems' => 0,
                'maxitems' => 1,
            ],

        ],
        'allowed_dates' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metalocation.allowed_dates',
            'config' => [
                'type' => 'text',
                'rows' => 5,
                'cols' => 3,
                'placeholder' => '17.4.2017' . LF . '23.5.2017'
            ],
        ],
        'description' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metalocation.description',
            'config' => [
                'type' => 'text',
                'rows' => 5,
                'cols' => 3,
            ],
        ],


        'accommodations' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metalocation.accommodations',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_courses_domain_model_metaaccommodation',
                'foreign_field' => 'metalocation',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],

        'transfers' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metalocation.transfers',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_courses_domain_model_metatransfer',
                'foreign_field' => 'metalocation',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
        'rooms' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metalocation.rooms',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_courses_domain_model_metaroom',
                'foreign_field' => 'metalocation',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
        'highseason_surcharge' => [
            'label' => 'LLL:EXT:courses/Resources/Private/Language/locallang_db.xlf:tx_courses_domain_model_metalocation.highseason_surcharge',
            'config' => [
                'type' => 'input',
                'default' => 0,
            ],
        ],
        'course' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
