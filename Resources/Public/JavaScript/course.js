jQuery(document).ready(function ($) {

	// $('.date-field').fdatepicker({
	// 	daysOfWeekDisabled: [0, 2, 3, 4, 5, 6],
	// 	language: "de",
	// 	weekStart: 1
	// });
	// $('.date-field').fdatepicker( "option", "dateFormat", 'yy-mm-dd' );
	var afterFilter = function (result) {
		$('#service_list').show();
		$('#start-information').hide();
	};
    //
	// $('#container-germanLevel').dependsOn({
	// 	'#field-learnedGerman': {
	// 		checked: true
	// 	}
	// });
	// $('#container-previousCourseLocation, #container-previousCourseYear').dependsOn({
	// 	'#field-previousCourse': {
	// 		checked: true
	// 	}
	// });
	//$('#price_filter').val('0-500');
	//$("#price_slider").slider({
	//  range:true,
	//  min: 0,
	//  max: 500,
	//  values:[0, 500],
	//  step: 5,
	//  slide: function(event, ui) {
	//    $("#price_range_label").html('$' + ui.values[ 0 ] + ' - $' + ui.values[ 1 ] );
	//    $('#price_filter').val(ui.values[0] + '-' + ui.values[1]).trigger('change');
	//  }
	//});
	//
	//$('#status :checkbox').prop('checked', true);
	FilterJS(courses, "#service_list", {
		template: '#template',
		callbacks: {
			afterFilter: afterFilter
		},
		criterias: [
			{field: 'title', ele: '#title_filter', all: 'all'},
			{field: 'accommodation', ele: '#accommodation_filter :checkbox'},
			{field: 'food', ele: '#food_filter :checkbox'},
			{field: 'location', ele: '#location_filter :checkbox'}
			// {field: 'room', ele: '#room_filter :checkbox'}
			//  {field: 'status', ele: '#status :checkbox'}
		]
		//search: { ele: '#search_box' }
	});


});
